// brunch-config.js
module.exports = {
  notifications: false,
  server: {
    // hostname: '0.0.0.0' // Allow access from other computers
  },
  files: {
    javascripts: {joinTo: 'js/app.js'},
    stylesheets: {joinTo: 'css/app.css'}
  },

  plugins: {
    babel: {
      presets: ['es2015']
    },
    elmBrunch: {
      // https://www.npmjs.com/package/elm-brunch
      mainModules: ['app/elm/Main.elm'],
      outputFolder: 'public/js',
      // makeParameters : ['--warn --debug']
      makeParameters : ['--warn']
    },
    sass: {
      options: {
        includePaths: [
          // 'node_modules/bootstrap/scss'
        ]
      }
    }
  }
}
