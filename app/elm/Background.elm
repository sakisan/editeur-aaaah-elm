module Background exposing (..)

import MapSvg exposing (mapSvg, ViewMessages, ViewParameters)
import Shapes.Element as Elements exposing (..)
import Shapes.ID as ID exposing (..)
import Shapes.Point exposing (..)
import Shapes.Shape as Shapes exposing (..)
import Shapes.StrokeWidth as StrokeWidth exposing (..)
import TimeControl as TimeControl exposing (..)
import Svg exposing (..)


backgroundImage : Point -> ViewMessages msg -> List (Svg msg)
backgroundImage offset svgMessages =
    let
        withOffset : Point -> Point
        withOffset =
            pointSum offset
    in
        -- cadre
        [ Rectangle
            { p1 = withOffset { x = 0, y = 0 }
            , p2 = withOffset { x = 800, y = 400 }
            , strokeWidth = StrokeWidth.fromInt 1
            , fill = False
            , id = ID.fromString "bg-cadre"
            }
          -- montagne
        , QuadraticCurve
            { p1 = withOffset { x = 700, y = 55 }
            , p2 = withOffset { x = 700, y = 400 }
            , p3 = Just <| withOffset { x = 683, y = 250 }
            , strokeWidth = StrokeWidth.fromInt 1
            , id = ID.fromString "bg-montagne"
            }
        , QuadraticCurve
            { p1 = withOffset { x = 700, y = 55 }
            , p2 = withOffset { x = 800, y = 55 }
            , p3 = Just <| withOffset { x = 750, y = 47 }
            , strokeWidth = StrokeWidth.fromInt 1
            , id = ID.fromString "bg-montagne-2"
            }
          -- pharmacie
        , Rectangle
            { p1 = withOffset { x = 740, y = 17 }
            , p2 = withOffset { x = 766, y = 52 }
            , strokeWidth = StrokeWidth.fromInt 1
            , fill = False
            , id = ID.fromString "bg-pharmacie"
            }
          -- platteforme coulante
        , QuadraticCurve
            { p1 = withOffset { x = 0, y = 360 }
            , p2 = withOffset { x = 180, y = 368 }
            , p3 = Just <| withOffset { x = 90, y = 355 }
            , strokeWidth = StrokeWidth.fromInt 1
            , id = ID.fromString "bg-platteforme"
            }
        , QuadraticCurve
            { p1 = withOffset { x = 180, y = 368 }
            , p2 = withOffset { x = 100, y = 400 }
            , p3 = Just <| withOffset { x = 114, y = 362 }
            , strokeWidth = StrokeWidth.fromInt 1
            , id = ID.fromString "bg-platteform-2"
            }
        ]
            |> List.map Elements.fromShape
            |> Elements.fromList
            |> mapSvg
                { class = "no-shape"
                , messages = svgMessages
                , classFor = (\id -> "")
                , time = TimeControl.zero
                }
