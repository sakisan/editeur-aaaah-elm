module Shapes.ID
    exposing
        ( ID
        , fromString
        , equals
        , compare
        , combine
        , contains
        )


type ID
    = ID String


fromString : String -> ID
fromString string =
    ID string


equals : ID -> ID -> Bool
equals (ID a) (ID b) =
    a == b


compare : ID -> ID -> Order
compare (ID a) (ID b) =
    Basics.compare a b


combine : ID -> ID -> ID
combine (ID a) (ID b) =
    fromString <| a ++ b


contains : String -> ID -> Bool
contains string (ID id) =
    String.contains string id
