module Shapes.Group
    exposing
        ( Group
        , insert
        , remove
        , removeByID
        , fromTwoShapes
        , fromList
        , toList
        , member
        , hasShape
        , hasShapeWithID
        , getShapeByID
        , getGroupID
        , setID
        , mapShapes
        , minX
        , minY
        , fold
        , getShapeA
        , rotationCenter
        )

import GenericSet exposing (..)
import Shapes.ID as ID exposing (ID)
import Shapes.Point as Points exposing (..)
import Shapes.Shape as Shape exposing (Shape)
import Utils exposing (..)


type Group
    = Group Shape Shape (GenericSet Shape)


minimalCapacity : Int
minimalCapacity =
    2


insert : Shape -> Group -> Group
insert shape (Group a b set) =
    if Shape.equals a shape then
        Group a b set
    else if Shape.equals b shape then
        Group a b set
    else
        Group a b (GenericSet.insert shape set)


getThirdShape : Group -> Maybe Shape
getThirdShape (Group a b set) =
    set
        |> GenericSet.toList
        |> List.head


remove : Shape -> Group -> Either Group Shape
remove shape group =
    let
        (Group a b set) =
            group
    in
        if Shape.equals a shape then
            case getThirdShape group of
                Just shape ->
                    GenericSet.remove shape set
                        |> Group shape b
                        |> This

                Nothing ->
                    That b
        else if Shape.equals b shape then
            case getThirdShape group of
                Just shape ->
                    GenericSet.remove shape set
                        |> Group a shape
                        |> This

                Nothing ->
                    That a
        else
            GenericSet.remove shape set
                |> Group a b
                |> This


removeByID : ID -> Group -> Either Group Shape
removeByID id group =
    case getShapeByID id group of
        Just shape ->
            remove shape group

        Nothing ->
            This group


fromTwoShapes : Shape -> Shape -> Maybe Group
fromTwoShapes a b =
    -- a and b could have the same ID, so this is Maybe a group...
    fromList [ a, b ]


fromSet : GenericSet Shape -> Maybe Group
fromSet set =
    -- this method is not exposed so we can assume the GenericSet has
    -- the proper comparer
    let
        a : Maybe Shape
        a =
            set
                |> GenericSet.toList
                |> List.head
    in
        case a of
            Nothing ->
                Nothing

            Just shapeA ->
                let
                    b : Maybe Shape
                    b =
                        set
                            |> GenericSet.remove shapeA
                            |> GenericSet.toList
                            |> List.head
                in
                    case b of
                        Nothing ->
                            Nothing

                        Just shapeB ->
                            set
                                |> GenericSet.remove shapeA
                                |> GenericSet.remove shapeB
                                |> Group shapeA shapeB
                                |> Just


fromList : List Shape -> Maybe Group
fromList list =
    GenericSet.fromList Shape.idComparer list
        |> fromSet


toList : Group -> List Shape
toList (Group a b set) =
    a :: b :: GenericSet.toList set


getGroupID : Group -> ID
getGroupID (Group a b set) =
    GenericSet.foldl
        (\shape id -> Shape.getShapeID shape |> ID.combine id)
        (ID.fromString "")
        set
        |> ID.combine (Shape.getShapeID a)
        |> ID.combine (Shape.getShapeID b)


setID : ID -> Group -> Group
setID id group =
    let
        mapped : Either Group Shape
        mapped =
            mapShapes
                (\shape ->
                    Shape.setID
                        (Shape.getShapeID shape
                            |> ID.combine id
                        )
                        shape
                )
                group
    in
        case mapped of
            This mappedGroup ->
                mappedGroup

            That mustNotHappen ->
                group


member : Shape -> Group -> Bool
member shape =
    hasShapeWithID (Shape.getShapeID shape)


hasShape : Shape -> Group -> Bool
hasShape =
    member


hasShapeWithID : ID -> Group -> Bool
hasShapeWithID id group =
    group
        |> toList
        |> List.any (Shape.matchID id)


getShapeByID : ID -> Group -> Maybe Shape
getShapeByID id (Group a b set) =
    if Shape.matchID id a then
        Just a
    else if Shape.matchID id b then
        Just b
    else
        GenericSet.toList set
            |> List.filter (Shape.matchID id)
            |> List.head


mapShapes : (Shape -> Shape) -> Group -> Either Group Shape
mapShapes function (Group a b set) =
    let
        mappedSet : Maybe Group
        mappedSet =
            set
                |> GenericSet.insert a
                |> GenericSet.insert b
                |> GenericSet.map Shape.idComparer function
                |> fromSet
    in
        case mappedSet of
            Just group ->
                This group

            Nothing ->
                That <| function a


minX : Group -> Float
minX =
    fold
        (\shape x ->
            Basics.min (Points.minX <| Shape.getPointsArray shape) x
        )
        500000


minY : Group -> Float
minY =
    fold
        (\shape y ->
            Basics.min (Points.minY <| Shape.getPointsArray shape) y
        )
        500000


maxX : Group -> Float
maxX =
    fold
        (\shape x ->
            Basics.max (Points.maxX <| Shape.getPointsArray shape) x
        )
        500000


maxY : Group -> Float
maxY =
    fold
        (\shape y ->
            Basics.max (Points.maxY <| Shape.getPointsArray shape) y
        )
        500000


fold : (Shape -> b -> b) -> b -> Group -> b
fold function first (Group shapeA shapeB set) =
    GenericSet.foldr function
        (first
            |> function shapeA
            |> function shapeB
        )
        set


getShapeA : Group -> Shape
getShapeA (Group a b set) =
    a


rotationCenter : Group -> Point
rotationCenter group =
    group
        |> toList
        |> List.map
            (\shape ->
                ( Shape.upperLeftPlusHalfWidth shape
                , Shape.lowerRightPlusHalfWidth shape
                )
            )
        |> List.foldr
            (\( upperLeftA, lowerRightA ) ( upperLeftB, lowerRightB ) ->
                ( { x = Basics.min upperLeftA.x upperLeftB.x
                  , y = Basics.min upperLeftA.y upperLeftB.y
                  }
                , { x = Basics.max lowerRightA.x lowerRightB.x
                  , y = Basics.max lowerRightA.y lowerRightB.y
                  }
                )
            )
            ( { x = 500000, y = 500000 }, { x = -500000, y = -500000 } )
        |> (\( upperLeft, lowerRightB ) ->
                { x = average upperLeft.x lowerRightB.x
                , y = average upperLeft.y lowerRightB.y
                }
           )
