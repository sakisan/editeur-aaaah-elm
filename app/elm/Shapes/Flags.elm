module Shapes.Flags
    exposing
        ( Flags
        , Flag
        , addFlag
        , targetFlag
        , actionFlag
        , handleFlag
        , subjectFlag
        , initialFlags
        , hasFlag
        , equals
        , compare
        , isCoreElement
        )

import Set exposing (Set)


type Flags
    = Flags (Set String)


type Flag
    = TranslateFlag
    | HandleFlag
    | SubjectFlag
    | ActionFlag


addFlag : Flag -> Flags -> Flags
addFlag flag (Flags set) =
    Set.insert (getFlagString flag) set
        |> Flags


targetFlag : Flag
targetFlag =
    TranslateFlag


handleFlag : Flag
handleFlag =
    HandleFlag


actionFlag : Flag
actionFlag =
    ActionFlag


subjectFlag : Flag
subjectFlag =
    SubjectFlag


getFlagString : Flag -> String
getFlagString flag =
    case flag of
        TranslateFlag ->
            "target"

        HandleFlag ->
            "handle"

        SubjectFlag ->
            "subject"

        ActionFlag ->
            "action"


initialFlags : Flags
initialFlags =
    Flags Set.empty


hasFlag : Flag -> Flags -> Bool
hasFlag flag (Flags set) =
    set
        |> Set.member (getFlagString flag)


equals : Flags -> Flags -> Bool
equals a b =
    compare a b == EQ


compare : Flags -> Flags -> Order
compare (Flags a) (Flags b) =
    Basics.compare (Set.foldr (++) "" a) (Set.foldr (++) "" b)


isCoreElement : Flags -> Bool
isCoreElement (Flags set) =
    -- subject to change
    Set.isEmpty set
