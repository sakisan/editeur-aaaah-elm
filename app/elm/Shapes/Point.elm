module Shapes.Point exposing (..)

import Array exposing (Array)


type alias Point =
    { x : Float
    , y : Float
    }



-- UTILS - points


bogusPoint : Point
bogusPoint =
    { x = 0, y = 0 }


pointDifference : Point -> Point -> Point
pointDifference p1 p2 =
    { x = p1.x - p2.x, y = p1.y - p2.y }


pointSum : Point -> Point -> Point
pointSum p1 p2 =
    { x = p1.x + p2.x, y = p1.y + p2.y }


foldPointArray : (b -> b -> b) -> (Point -> b) -> b -> Array Point -> b
foldPointArray folder mapper default array =
    let
        first =
            case Array.get 1 array of
                Nothing ->
                    default

                Just p ->
                    mapper p
    in
        Array.foldr (\p o -> folder (mapper p) o) first array


defaultPoint : Point
defaultPoint =
    { x = 0
    , y = 0
    }


p1 : Array Point -> Point
p1 points =
    Array.get 0 points
        |> Maybe.withDefault defaultPoint


p2 : Array Point -> Point
p2 points =
    Array.get 1 points
        |> Maybe.withDefault defaultPoint


isInside : Point -> Point -> Point -> Bool
isInside p1 p2 point =
    (point.x > Basics.min p1.x p2.x)
        && (point.x < Basics.max p1.x p2.x)
        && (point.y > Basics.min p1.y p2.y)
        && (point.y < Basics.max p1.y p2.y)

minX : Array Point -> Float
minX points =
    foldPointArray Basics.min .x 0 points


minY : Array Point -> Float
minY points =
    foldPointArray Basics.min .y 0 points

maxX : Array Point -> Float
maxX points =
    foldPointArray Basics.max .x 0 points


maxY : Array Point -> Float
maxY points =
    foldPointArray Basics.max .y 0 points


shapeWidth : Array Point -> Float
shapeWidth points =
    minX points
        |> (-) (foldPointArray Basics.max .x 0 points)


shapeHeight : Array Point -> Float
shapeHeight points =
    minY points
        |> (-) (foldPointArray Basics.max .y 0 points)


middleX : Array Point -> Float
middleX points =
    minX points + shapeWidth points / 2


middleY : Array Point -> Float
middleY points =
    minY points + shapeHeight points / 2
