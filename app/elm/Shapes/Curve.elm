module Shapes.Curve exposing (..)

import Shapes.Point as Points exposing (..)


rotationCenterCurve : Point -> Point -> Point -> Point
rotationCenterCurve start point end =
    let
        min : Point
        min =
            upperLeft start point end

        max : Point
        max =
            lowerRight start point end
    in
        { x = (min.x + max.x) / 2
        , y = (min.y + max.y) / 2
        }


evaluate : Float -> Float -> Float -> Float -> Float
evaluate t start end point =
    -- 0 <= t <= 1
    (1 - t)
        * ((1 - t) * start + t * point)
        + t
        * ((1 - t) * point + t * end)


evaluatePoint : Point -> Point -> Point -> Float -> Point
evaluatePoint start point end t =
    { x = evaluate t start.x point.x end.x
    , y = evaluate t start.y point.y end.y
    }


list : Point -> Point -> Point -> List Point
list start point end =
    List.range 0 1000
        |> List.map toFloat
        |> List.map (\f -> f / 1000)
        |> List.map (evaluatePoint start point end)


lowerRight : Point -> Point -> Point -> Point
lowerRight start point end =
    list start point end
        |> List.foldr
            (\p1 p2 ->
                { x = Basics.max p1.x p2.x
                , y = Basics.max p1.y p2.y
                }
            )
            { x = -5000000
            , y = -5000000
            }


upperLeft : Point -> Point -> Point -> Point
upperLeft start point end =
    list start point end
        |> List.foldr
            (\p1 p2 ->
                { x = Basics.min p1.x p2.x
                , y = Basics.min p1.y p2.y
                }
            )
            { x = 5000000
            , y = 5000000
            }
