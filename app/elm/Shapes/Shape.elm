module Shapes.Shape exposing (..)

import Shapes.Curve as Curve exposing (..)
import Shapes.ID as ID exposing (..)
import Shapes.Point as Point exposing (..)
import Shapes.StrokeWidth as StrokeWidth exposing (StrokeWidth, toInt)
import Utils exposing (..)


-- elm-lang packages

import Array exposing (Array)


-- MODEL


type Shape
    = Rectangle TwoPointsFillShape
    | Ellipse TwoPointsFillShape
    | Line TwoPointsShape
    | QuadraticCurve ThreePointsShape
    | Polygon PointsFillShape


type ShapeType
    = RectangleType
    | EllipseType
    | LineType
    | QuadraticCurveType
    | PolygonType


type alias TwoPointsShape =
    WithTwoPoints (WithCommonProperties {})


type alias TwoPointsFillShape =
    WithTwoPoints (WithFillProperties (WithCommonProperties {}))


type alias ThreePointsShape =
    WithThreePoints (WithCommonProperties {})


type alias ThreePointsFillShape =
    WithThreePoints (WithFillProperties (WithCommonProperties {}))


type alias PointsFillShape =
    WithPoints (WithFillProperties (WithCommonProperties {}))


type alias WithPoints a =
    { a | points : Array Point }


type alias WithTwoPoints a =
    { a | p1 : Point, p2 : Point }


type alias WithThreePoints a =
    WithTwoPoints { a | p3 : Maybe Point }


type alias WithCommonProperties a =
    { a | strokeWidth : StrokeWidth, id : ID }


type alias WithFillProperties a =
    { a | fill : Bool }


type alias TypeAndAllProperties =
    WithCommonProperties
        (WithFillProperties
            { shapeType : ShapeType
            }
        )



-- hardcoded mappings


getType : Shape -> ShapeType
getType shape =
    case shape of
        Rectangle a ->
            RectangleType

        Ellipse a ->
            EllipseType

        Line a ->
            LineType

        QuadraticCurve a ->
            QuadraticCurveType

        Polygon a ->
            PolygonType



{- The types are necessary to ensure the model is correct,
   but they can't be used to be matched against.
-}


maxNumberOfPoints : ShapeType -> Maybe Int
maxNumberOfPoints shapeType =
    case shapeType of
        RectangleType ->
            Just 2

        EllipseType ->
            Just 2

        LineType ->
            Just 2

        QuadraticCurveType ->
            Just 3

        PolygonType ->
            Nothing



-- get shape info


getCommonProperties : Shape -> WithCommonProperties {}
getCommonProperties shape =
    let
        toCommon : WithCommonProperties b -> WithCommonProperties {}
        toCommon p =
            { strokeWidth = p.strokeWidth
            , id = p.id
            }
    in
        case shape of
            Rectangle p ->
                toCommon p

            Ellipse p ->
                toCommon p

            Line p ->
                toCommon p

            QuadraticCurve p ->
                toCommon p

            Polygon p ->
                toCommon p


getFill : Shape -> Bool
getFill shape =
    case shape of
        Rectangle p ->
            p.fill

        Ellipse p ->
            p.fill

        Line p ->
            False

        QuadraticCurve p ->
            False

        Polygon p ->
            p.fill


getShapeID : Shape -> ID
getShapeID shape =
    getCommonProperties shape |> .id


pointLimitReached : Shape -> Bool
pointLimitReached shape =
    case maxNumberOfPoints (getType shape) of
        Nothing ->
            False

        Just n ->
            n == currentNumberOfPoints shape


getPointsArrayFrom2P : WithTwoPoints a -> Array Point
getPointsArrayFrom2P p =
    Array.fromList [ p.p1, p.p2 ]


getPointsArrayFrom3P : WithThreePoints a -> Array Point
getPointsArrayFrom3P p =
    case p.p3 of
        Just p3 ->
            Array.push p3 <| getPointsArrayFrom2P p

        Nothing ->
            getPointsArrayFrom2P p


getPointsArray : Shape -> Array Point
getPointsArray shape =
    case shape of
        Rectangle a ->
            getPointsArrayFrom2P a

        Ellipse a ->
            getPointsArrayFrom2P a

        Line a ->
            getPointsArrayFrom2P a

        QuadraticCurve a ->
            getPointsArrayFrom3P a

        Polygon a ->
            a.points


currentNumberOfPoints : Shape -> Int
currentNumberOfPoints shape =
    getPointsArray shape
        |> Array.length



-- update shapes


updateShapeLastPoint : Point -> Shape -> Shape
updateShapeLastPoint point shape =
    case shape of
        Rectangle a ->
            Rectangle { a | p2 = point }

        Ellipse a ->
            Ellipse { a | p2 = point }

        Line a ->
            Line { a | p2 = point }

        QuadraticCurve a ->
            case a.p3 of
                Just p3 ->
                    QuadraticCurve { a | p3 = Just point }

                Nothing ->
                    QuadraticCurve { a | p2 = point }

        Polygon a ->
            Polygon { a | points = updateLast a.points point }


replacePoints : Shape -> Array Point -> Shape
replacePoints shape points =
    let
        replace2P : WithTwoPoints a -> (WithTwoPoints a -> Shape) -> Shape
        replace2P a shapeFunction =
            shapeFunction
                { a
                    | p1 = Array.get 0 points |> Maybe.withDefault a.p1
                    , p2 = Array.get 1 points |> Maybe.withDefault a.p2
                }

        replace3P : WithThreePoints a -> (WithThreePoints a -> Shape) -> Shape
        replace3P a shapeFunction =
            shapeFunction
                { a
                    | p1 = Array.get 0 points |> Maybe.withDefault a.p1
                    , p2 = Array.get 1 points |> Maybe.withDefault a.p2
                    , p3 = Array.get 2 points
                }
    in
        case shape of
            Rectangle a ->
                replace2P a Rectangle

            Ellipse a ->
                replace2P a Ellipse

            Line a ->
                replace2P a Line

            QuadraticCurve a ->
                replace3P a QuadraticCurve

            Polygon a ->
                Polygon { a | points = points }


insertPointToShape : Point -> Shape -> Shape
insertPointToShape point shape =
    if pointLimitReached shape then
        updateShapeLastPoint point shape
    else
        getPointsArray shape
            |> Array.push point
            |> replacePoints shape


updateAllPoints : (Point -> Point) -> Shape -> Shape
updateAllPoints updater shape =
    getPointsArray shape
        |> Array.map updater
        |> replacePoints shape


moveShape : Point -> Shape -> Shape
moveShape point =
    updateAllPoints <| pointSum point


setID : ID -> Shape -> Shape
setID id shape =
    case shape of
        Rectangle a ->
            Rectangle { a | id = id }

        Ellipse a ->
            Ellipse { a | id = id }

        Line a ->
            Line { a | id = id }

        QuadraticCurve a ->
            QuadraticCurve { a | id = id }

        Polygon a ->
            Polygon { a | id = id }


updateFill : Bool -> Shape -> Shape
updateFill fill shape =
    case shape of
        Rectangle p ->
            Rectangle { p | fill = fill }

        Ellipse p ->
            Ellipse { p | fill = fill }

        Line p ->
            shape

        QuadraticCurve p ->
            shape

        Polygon p ->
            Polygon { p | fill = fill }


setStrokeWidth : StrokeWidth -> Shape -> Shape
setStrokeWidth strokeWidth shape =
    case shape of
        Rectangle p ->
            Rectangle { p | strokeWidth = strokeWidth }

        Ellipse p ->
            Ellipse { p | strokeWidth = strokeWidth }

        Line p ->
            Line { p | strokeWidth = strokeWidth }

        QuadraticCurve p ->
            QuadraticCurve { p | strokeWidth = strokeWidth }

        Polygon p ->
            Polygon { p | strokeWidth = strokeWidth }


mapPoints : (Int -> Point -> Point) -> Shape -> Shape
mapPoints function shape =
    shape
        |> getPointsArray
        |> Array.indexedMap function
        |> replacePoints shape


setX1 : Float -> Shape -> Shape
setX1 value =
    mapPoints
        (\index point ->
            if index == 0 then
                { point | x = value }
            else
                point
        )


setY1 : Float -> Shape -> Shape
setY1 value =
    mapPoints
        (\index point ->
            if index == 0 then
                { point | y = value }
            else
                point
        )



setX2 : Float -> Shape -> Shape
setX2 value =
    mapPoints
        (\index point ->
            if index == 1 then
                { point | x = value }
            else
                point
        )



setY2 : Float -> Shape -> Shape
setY2 value =
    mapPoints
        (\index point ->
            if index == 1 then
                { point | y = value }
            else
                point
        )



-- Create a new shape with one starting point


newShape : TypeAndAllProperties -> Point -> Shape
newShape properties startingPoint =
    let
        new2P : (TwoPointsShape -> Shape) -> Shape
        new2P shapeFunction =
            shapeFunction
                { id = properties.id
                , strokeWidth = properties.strokeWidth
                , p1 = startingPoint
                , p2 = startingPoint
                }

        new2PF : (TwoPointsFillShape -> Shape) -> Shape
        new2PF shapeFunction =
            shapeFunction
                { id = properties.id
                , strokeWidth = properties.strokeWidth
                , fill = properties.fill
                , p1 = startingPoint
                , p2 = startingPoint
                }

        new3P : (ThreePointsShape -> Shape) -> Shape
        new3P shapeFunction =
            shapeFunction
                { id = properties.id
                , strokeWidth = properties.strokeWidth
                , p1 = startingPoint
                , p2 = startingPoint
                , p3 = Nothing
                }

        newNP : (PointsFillShape -> Shape) -> Shape
        newNP shapeFunction =
            shapeFunction
                { id = properties.id
                , strokeWidth = properties.strokeWidth
                , fill = properties.fill
                , points = Array.fromList [ startingPoint, startingPoint ]
                }
    in
        case properties.shapeType of
            RectangleType ->
                new2PF Rectangle

            EllipseType ->
                new2PF Ellipse

            LineType ->
                new2P Line

            QuadraticCurveType ->
                new3P QuadraticCurve

            PolygonType ->
                newNP Polygon


circle : Int -> WithCommonProperties (WithFillProperties {}) -> Point -> Shape
circle radius properties point =
    let
        sin45 : Float
        sin45 =
            sin (pi / 4)

        project : Int -> Float
        project radius =
            radius
                |> toFloat
                |> (*) sin45

        point1 : Point
        point1 =
            { x = point.x + (project radius)
            , y = point.y + (project radius)
            }

        point2 : Point
        point2 =
            { x = point.x - (project radius)
            , y = point.y - (project radius)
            }
    in
        newShape
            { id = properties.id
            , strokeWidth = properties.strokeWidth
            , fill = properties.fill
            , shapeType = EllipseType
            }
            point1
            |> updateShapeLastPoint point2


review : Shape -> Shape
review shape =
    case shape of
        Polygon p ->
            if Array.length p.points == 2 then
                Line
                    { p1 = p1 p.points
                    , p2 = p2 p.points
                    , strokeWidth = p.strokeWidth
                    , id = p.id
                    }
            else
                shape

        _ ->
            shape



-- UTILS - shapes


origin : Shape -> Point
origin shape =
    case shape of
        Line p ->
            p.p1

        QuadraticCurve p ->
            p.p1

        Polygon p ->
            p1 p.points

        _ ->
            { x = minX <| getPointsArray shape
            , y = minY <| getPointsArray shape
            }


idComparer : Shape -> Shape -> Order
idComparer a b =
    ID.compare (getShapeID a) (getShapeID b)


matchID : ID -> Shape -> Bool
matchID id shape =
    ID.equals id (getShapeID shape)


equals : Shape -> Shape -> Bool
equals a b =
    ID.equals (getShapeID a) (getShapeID b)


countsFor : Shape -> Int
countsFor shape =
    case shape of
        Polygon p ->
            Array.length p.points - 1

        _ ->
            1


rotationCenter : Shape -> Point
rotationCenter shape =
    let
        regularCenter : Point
        regularCenter =
            { x = middleX <| getPointsArray shape
            , y = middleY <| getPointsArray shape
            }

        centerByShape : Point
        centerByShape =
            case shape of
                QuadraticCurve curve ->
                    let
                        p3 : Point
                        p3 =
                            Maybe.withDefault curve.p1 curve.p3
                    in
                        if isPresent curve.p3 then
                            rotationCenterCurve curve.p1 curve.p2 p3
                        else
                            regularCenter

                _ ->
                    regularCenter
    in
        centerByShape
            |> shiftCenterForWidth shape


shiftCenterForWidth : Shape -> Point -> Point
shiftCenterForWidth shape point =
    point
        |> pointSum { x = halfWidth shape, y = halfWidth shape }


halfWidth : Shape -> Float
halfWidth shape =
    shape
        |> getCommonProperties
        |> .strokeWidth
        |> StrokeWidth.toInt
        |> toFloat
        |> (\w -> w / 2)


upperLeft : Shape -> Point
upperLeft shape =
    case shape of
        QuadraticCurve p ->
            let
                ( start, point, end ) =
                    get3Points shape
            in
                Curve.upperLeft start point end

        _ ->
            { x = getPointsArray shape |> minX
            , y = getPointsArray shape |> minY
            }


lowerRight : Shape -> Point
lowerRight shape =
    case shape of
        QuadraticCurve p ->
            let
                ( start, point, end ) =
                    get3Points shape
            in
                Curve.upperLeft start point end

        _ ->
            { x = getPointsArray shape |> maxX
            , y = getPointsArray shape |> maxY
            }


upperLeftPlusHalfWidth : Shape -> Point
upperLeftPlusHalfWidth shape =
    upperLeft shape
        |> pointSum { x = halfWidth shape, y = halfWidth shape }


lowerRightPlusHalfWidth : Shape -> Point
lowerRightPlusHalfWidth shape =
    lowerRight shape
        |> pointSum { x = halfWidth shape, y = halfWidth shape }


get3Points : Shape -> ( Point, Point, Point )
get3Points shape =
    -- this is only meant for curves at this point
    case shape of
        QuadraticCurve p ->
            ( p.p1, p.p3 |> Maybe.withDefault p.p1, p.p2 )

        _ ->
            ( { x = 0, y = 0 }, { x = 0, y = 0 }, { x = 0, y = 0 } )
