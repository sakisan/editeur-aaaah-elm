module Shapes.Element
    exposing
        ( Element
        , ElementSet
        , ShapeInfo
        , MyElement(..)
        , empty
        , elementsComparer
        , toList
        , getShapeInfos
        , getGroupsTupled
        , getGroups
        , getShapes
        , getShape
        , getShape2
        , getAllShapes
        , getAllShapesSingle
        , count
        , mapElements
        , mapShapes
        , mapShapesSingle
        , insertShape
        , insertGroup
        , insert
        , getElementByShape
        , getCoreElementByShape
        , getElement
        , hasShape
        , hasShapeSingle
        , removeElement
        , removeShape
        , removeAll
        , fromShape
        , fromGroup
        , fromList
        , isGrouped
        , equals
        , adjustTranslation
        , flagAs
        , hasFullGroup
        , origin
        , rotationCenter
        , withOrder
        , withRotation
        , withTranslation
        , withAnimationsFrom
        , withNoAnimations
        , setID
        )

import GenericSet exposing (..)
import Shapes.Animation as Animation exposing (..)
import Shapes.Flags as Flags exposing (Flags, Flag)
import Shapes.Group as Group exposing (..)
import Shapes.ID as ID exposing (..)
import Shapes.Point as Point exposing (..)
import Shapes.Shape as Shapes exposing (..)
import Utils exposing (..)


type alias Element =
    { element : MyElement
    , translation : Maybe Translate
    , rotation : Maybe Rotate
    , flags : Flags
    }


type MyElement
    = ShapeElement Shape
    | GroupElement Group


type ElementSet
    = ElementSet (GenericSet Element)


type alias ShapeInfo =
    { shape : Shape
    , element : Element
    , isGrouped : Bool
    }


empty : ElementSet
empty =
    GenericSet.empty elementsComparer
        |> ElementSet


elementsComparer : Element -> Element -> Order
elementsComparer a b =
    let
        idOrder : Order
        idOrder =
            ID.compare (getElementID a) (getElementID b)
    in
        case idOrder of
            EQ ->
                Flags.compare a.flags b.flags

            _ ->
                idOrder


getElementID : Element -> ID
getElementID element =
    case element.element of
        ShapeElement shape ->
            Shapes.getShapeID shape

        GroupElement group ->
            Group.getGroupID group


toList : ElementSet -> List Element
toList (ElementSet set) =
    GenericSet.toList set


toFilteredList : (Element -> Maybe a) -> ElementSet -> List a
toFilteredList map elements =
    elements
        |> toList
        |> List.foldr
            (\element list ->
                case map element of
                    Just a ->
                        a :: list

                    Nothing ->
                        list
            )
            []


getAllShapes : ElementSet -> List Shape
getAllShapes elements =
    elements
        |> toList
        |> List.map getAllShapesSingle
        |> List.concat


count : ElementSet -> Int
count elements =
    elements
        |> getAllShapes
        |> List.map Shapes.countsFor
        |> List.foldr (+) 0


getAllShapesSingle : Element -> List Shape
getAllShapesSingle element =
    case element.element of
        ShapeElement shape ->
            [ shape ]

        GroupElement group ->
            Group.toList group


getShapeInfos : ElementSet -> List ShapeInfo
getShapeInfos =
    toFilteredList
        (\element ->
            case element.element of
                ShapeElement shape ->
                    Just
                        { shape = shape
                        , element = element
                        , isGrouped = False
                        }

                _ ->
                    Nothing
        )


getGroupsTupled : ElementSet -> List ( Group, Element )
getGroupsTupled =
    toFilteredList
        (\element ->
            case element.element of
                GroupElement group ->
                    Just ( group, element )

                _ ->
                    Nothing
        )


getShapes : ElementSet -> List Shape
getShapes =
    toFilteredList
        (\element ->
            case element.element of
                ShapeElement shape ->
                    Just shape

                _ ->
                    Nothing
        )


getShape : Shape -> ElementSet -> Maybe Shape
getShape shape elements =
    getShapes elements
        |> List.filter (Shapes.equals shape)
        |> List.head


getShape2 : Shape -> ElementSet -> Shape
getShape2 shape elements =
    getShape shape elements
        |> Maybe.withDefault shape


getGroups : ElementSet -> List Group
getGroups =
    toFilteredList
        (\element ->
            case element.element of
                GroupElement group ->
                    Just group

                _ ->
                    Nothing
        )


mapElements : (Element -> Element) -> ElementSet -> ElementSet
mapElements function elements =
    elements
        |> toList
        |> List.map function
        |> fromList


mapShapes : (Shape -> Shape) -> ElementSet -> ElementSet
mapShapes function elements =
    elements
        |> toList
        |> List.map (mapShapesSingle function)
        |> fromList


mapShapesSingle : (Shape -> Shape) -> Element -> Element
mapShapesSingle function element =
    let
        myElement : MyElement
        myElement =
            case element.element of
                ShapeElement shape ->
                    ShapeElement (function shape)

                GroupElement group ->
                    case Group.mapShapes function group of
                        This group ->
                            GroupElement group

                        That shape ->
                            ShapeElement shape
    in
        { element | element = myElement }


insertShape : Shape -> ElementSet -> ElementSet
insertShape shape =
    ShapeElement shape
        |> fromMyElement
        |> insert


insertGroup : Group -> ElementSet -> ElementSet
insertGroup group =
    GroupElement group
        |> fromMyElement
        |> insert


insert : Element -> ElementSet -> ElementSet
insert element (ElementSet set) =
    GenericSet.insert element set
        |> ElementSet


getElementByID : ID -> ElementSet -> Maybe Element
getElementByID id elements =
    elements
        |> toList
        |> List.filter (containsID id)
        |> List.head


getCoreElementByShape : Shape -> ElementSet -> Maybe Element
getCoreElementByShape shape elements =
    getElementByShape shape elements
        |> List.filter (\e -> Flags.isCoreElement e.flags)
        |> List.head


getElementByShape : Shape -> ElementSet -> List Element
getElementByShape shape elements =
    elements
        |> toList
        |> List.filter (hasShapeSingle shape)


getElement : Element -> ElementSet -> Maybe Element
getElement element =
    getElementByID (getElementID element)


containsID : ID -> Element -> Bool
containsID id element =
    case element.element of
        ShapeElement shape ->
            ID.equals (Shapes.getShapeID shape) id

        GroupElement group ->
            if ID.equals (Group.getGroupID group) id then
                True
            else
                Group.hasShapeWithID id group


hasShape : Shape -> ElementSet -> Bool
hasShape shape elements =
    elements
        |> toList
        |> List.any (hasShapeSingle shape)


hasShapeSingle : Shape -> Element -> Bool
hasShapeSingle shape element =
    case element.element of
        ShapeElement myShape ->
            Shapes.equals shape myShape

        GroupElement group ->
            Group.toList group
                |> List.any (Shapes.equals shape)


removeElement : Element -> ElementSet -> ElementSet
removeElement element elements =
    elements
        |> toList
        |> List.filter (not << equals element)
        |> fromList


removeShape : Shape -> ElementSet -> ElementSet
removeShape shape elements =
    elements
        |> toFilteredList (removeShapeSingle shape)
        |> GenericSet.fromList elementsComparer
        |> ElementSet


removeShapeSingle : Shape -> Element -> Maybe Element
removeShapeSingle shape element =
    let
        myElement : Maybe MyElement
        myElement =
            case element.element of
                ShapeElement shapeElement ->
                    if Shapes.equals shape shapeElement then
                        Nothing
                    else
                        Just <| ShapeElement shapeElement

                GroupElement group ->
                    if Group.hasShape shape group then
                        case Group.remove shape group of
                            This updatedGroup ->
                                Just <| GroupElement updatedGroup

                            That updatedShape ->
                                Just <| ShapeElement updatedShape
                    else
                        Just <| GroupElement group
    in
        Maybe.map (\e -> { element | element = e }) myElement


removeAll : List Shape -> ElementSet -> ElementSet
removeAll shapes elements =
    shapes
        |> List.foldr removeShape elements


fromShape : Shape -> Element
fromShape shape =
    ShapeElement shape
        |> fromMyElement


fromGroup : Group -> Element
fromGroup group =
    GroupElement group
        |> fromMyElement


fromMyElement : MyElement -> Element
fromMyElement element =
    { element = element
    , translation = Nothing
    , rotation = Nothing
    , flags = Flags.initialFlags
    }


fromList : List Element -> ElementSet
fromList =
    List.foldr insert empty


isGrouped : Shape -> ElementSet -> Bool
isGrouped shape elements =
    case getCoreElementByShape shape elements of
        Just element ->
            case element.element of
                GroupElement group ->
                    True

                ShapeElement shape ->
                    False

        Nothing ->
            False


equals : Element -> Element -> Bool
equals a b =
    ID.equals (getElementID a) (getElementID b)
        && Flags.equals a.flags b.flags


adjustTranslation : Point -> Element -> Element
adjustTranslation point element =
    { element
        | translation =
            (Just <| Animation.adjustTranslation point element.translation)
    }


flagAs : Flag -> Element -> Element
flagAs flag element =
    { element | flags = Flags.addFlag flag element.flags }


hasFullGroup : Group -> Element -> Bool
hasFullGroup group element =
    case element.element of
        ShapeElement shape ->
            False

        GroupElement myGroup ->
            myGroup
                |> Group.toList
                |> List.all (\shape -> Group.member shape group)


origin : Element -> Point
origin element =
    case element.element of
        ShapeElement shape ->
            Shapes.origin shape

        GroupElement shape ->
            { x = Group.minX shape
            , y = Group.minY shape
            }


rotationCenter : Element -> Point
rotationCenter element =
    case element.element of
        ShapeElement shape ->
            Shapes.rotationCenter shape

        GroupElement group ->
            Group.rotationCenter group


withOrder : (Element -> Element -> Order) -> ElementSet -> ElementSet
withOrder comparer elements =
    elements
        |> toList
        |> List.foldl insert
            (GenericSet.empty comparer
                |> ElementSet
            )


withRotation : Maybe Rotate -> Element -> Element
withRotation rotation element =
    { element | rotation = rotation }


withTranslation : Maybe Translate -> Element -> Element
withTranslation translation element =
    { element | translation = translation }


withAnimationsFrom : Element -> Element -> Element
withAnimationsFrom source =
    (withRotation source.rotation) >> (withTranslation source.translation)


withNoAnimations : Element -> Element
withNoAnimations =
    (withRotation Nothing) >> (withTranslation Nothing)


setID : ID -> Element -> Element
setID id element =
    { element
        | element =
            case element.element of
                ShapeElement shape ->
                    Shapes.setID id shape
                        |> ShapeElement

                GroupElement group ->
                    Group.setID id group
                        |> GroupElement
    }
