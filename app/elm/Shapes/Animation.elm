module Shapes.Animation exposing (..)

import Angle exposing (..)
import Shapes.Point exposing (..)
import TimeControl exposing (..)
import Utils exposing (..)


type Translate
    = Translate TranslateProperties


type Rotate
    = Rotate RotateProperties


type Loop
    = NoLoop
    | Restart
    | Return


type alias WithAnimationProperties a =
    { a | loop : Loop, delay : Time, time : Time }


type alias TranslateProperties =
    WithAnimationProperties { relativeTarget : Point }


type alias RotateProperties =
    WithAnimationProperties { angle : Angle }


getTranslate : TranslateProperties -> Translate
getTranslate =
    Translate


getRotate : RotateProperties -> Rotate
getRotate =
    Rotate


adjustTranslation : Point -> Maybe Translate -> Translate
adjustTranslation point maybe =
    case maybe of
        Nothing ->
            Translate
                { loop = NoLoop
                , delay = Time 0
                , time = Time 5
                , relativeTarget = point
                }

        Just (Translate properties) ->
            Translate
                { properties
                    | relativeTarget =
                        pointSum properties.relativeTarget point
                }


getRelativeTarget : Translate -> Point
getRelativeTarget (Translate translate) =
    translate.relativeTarget


setRelativeTarget : Point -> Translate -> Translate
setRelativeTarget p (Translate translate) =
    Translate { translate | relativeTarget = p }


toRotation : Point -> Point -> Point -> Rotate
toRotation startPoint endPoint center =
    Rotate
        { loop = NoLoop
        , delay = Time 0
        , time = Time 5
        , angle =
            Utils.angle
                (pointDifference startPoint center)
                (pointDifference endPoint center)
                |> fromRadians
        }


fromRadians : Float -> Angle
fromRadians angle =
    Angle angle


fromDegrees : Float -> Angle
fromDegrees angle =
    Angle (angle * (pi / 180))


toDegrees : Angle -> Float
toDegrees (Angle radians) =
    radians * (180 / pi)


addAngle : Maybe Rotate -> Rotate -> Rotate
addAngle maybe (Rotate rotate) =
    case maybe of
        Just (Rotate another) ->
            Rotate
                { rotate
                    | angle =
                        Angle.addAngle rotate.angle another.angle
                }

        Nothing ->
            Rotate rotate


toLoop : Int -> Loop
toLoop loop =
    case loop of
        1 ->
            Restart

        2 ->
            Return

        _ ->
            NoLoop
