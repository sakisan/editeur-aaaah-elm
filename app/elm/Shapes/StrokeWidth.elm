module Shapes.StrokeWidth
    exposing
        ( StrokeWidth
        , minimalWidth
        , maximalWidth
        , toInt
        , fromInt
        , fromString
        , add
        , addFromInt
        , toStringRaw
        , toString_
        )

import Utils exposing (..)


type StrokeWidth
    = StrokeWidth Int


minimalWidth : StrokeWidth
minimalWidth =
    StrokeWidth 0


maximalWidth : StrokeWidth
maximalWidth =
    StrokeWidth 255


isZero : StrokeWidth -> Bool
isZero (StrokeWidth width) =
    width == 0


toInt : StrokeWidth -> Int
toInt (StrokeWidth w) =
    w


fromInt : Int -> StrokeWidth
fromInt i =
    StrokeWidth i
        |> inBoundary


fromString : String -> StrokeWidth -> StrokeWidth
fromString string (StrokeWidth default) =
    tryToInt string default
        |> StrokeWidth
        |> inBoundary


isGreaterThan : StrokeWidth -> StrokeWidth -> Bool
isGreaterThan (StrokeWidth a) (StrokeWidth b) =
    a > b


isLessThan : StrokeWidth -> StrokeWidth -> Bool
isLessThan (StrokeWidth a) (StrokeWidth b) =
    a < b


add : StrokeWidth -> StrokeWidth -> StrokeWidth
add =
    map2 (+)


addFromInt : Int -> StrokeWidth -> StrokeWidth
addFromInt i (StrokeWidth a) =
    i
        + a
        |> StrokeWidth
        |> inBoundary


inBoundary : StrokeWidth -> StrokeWidth
inBoundary test =
    if isGreaterThan minimalWidth test then
        minimalWidth
    else if isLessThan maximalWidth test then
        maximalWidth
    else
        test


map : (Int -> Int) -> StrokeWidth -> StrokeWidth
map f (StrokeWidth i) =
    f i
        |> StrokeWidth
        |> inBoundary


map2 : (Int -> Int -> Int) -> StrokeWidth -> StrokeWidth -> StrokeWidth
map2 f (StrokeWidth a) (StrokeWidth b) =
    f a b
        |> StrokeWidth
        |> inBoundary


toStringRaw : StrokeWidth -> String
toStringRaw (StrokeWidth a) =
    Basics.toString a


toString_ : StrokeWidth -> String
toString_ (StrokeWidth a) =
    (if a == 0 then
        1
     else
        a
    )
        |> Basics.toString
