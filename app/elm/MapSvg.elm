module MapSvg exposing (mapSvg, ViewParameters, ViewMessages)

--internal packages

import Angle as Angle exposing (..)
import Shapes.Animation as Animation exposing (..)
import Shapes.Element as Elements exposing (..)
import Shapes.Flags as Flags exposing (..)
import Shapes.Group as Group exposing (..)
import Shapes.Point as Points exposing (..)
import Shapes.Shape as Shapes exposing (..)
import Shapes.StrokeWidth as StrokeWidth
import TimeControl as TimeControl exposing (..)
import Utils exposing (..)


-- elm-lang packages

import Array exposing (Array)
import Svg exposing (..)
import Svg.Attributes exposing (..)


type alias ViewParameters msg =
    { messages : ViewMessages msg
    , class : String
    , classFor : ShapeInfo -> String
    , time : Time
    }


type alias ViewMessages msg =
    { shapeDown : ShapeInfo -> Position -> msg
    , shapeUp : ShapeInfo -> Position -> msg
    }


mapSvg : ViewParameters msg -> ElementSet -> List (Svg msg)
mapSvg params elements =
    []
        ++ (elements
                |> Elements.getGroupsTupled
                |> List.map (groupView params)
           )
        ++ (elements
                |> Elements.getShapeInfos
                |> List.map (shapeView params)
           )


shapeView : ViewParameters msg -> ShapeInfo -> Svg msg
shapeView viewParams shapeInfo =
    case shapeInfo.shape of
        Rectangle rectangle ->
            rectangleView rectangle viewParams shapeInfo

        Ellipse ellipse ->
            ellipseView ellipse viewParams shapeInfo

        Line line ->
            lineView line viewParams shapeInfo

        QuadraticCurve curve ->
            quadraticCurveView curve viewParams shapeInfo

        Polygon polygon ->
            polygonView polygon viewParams shapeInfo


commonAttributes : WithCommonProperties a -> ViewParameters msg -> ShapeInfo -> List (Attribute msg)
commonAttributes properties params shapeInfo =
    [ stroke "#000000"
    , strokeWidth <| StrokeWidth.toString_ <| properties.strokeWidth
    , mouseUp <| params.messages.shapeUp shapeInfo
    , mouseDown <| params.messages.shapeDown shapeInfo
    , params.class
        |> appendIf (not <| Shapes.getFill shapeInfo.shape) " no-fill"
        |> String.append (params.classFor shapeInfo)
        |> class
    ]
        ++ (case shapeInfo.shape of
                Rectangle p ->
                    [ strokeLinecap "square"
                    , strokeLinejoin "mitter"
                    ]

                _ ->
                    [ strokeLinecap "round"
                    , strokeLinejoin "round"
                    ]
           )
        ++ (if shapeInfo.isGrouped then
                []
            else
                addAnimation params.time shapeInfo.element
           )


addAnimation : Time -> Element -> List (Attribute msg)
addAnimation time element =
    (addRotation time element)
        |> appendMaybes (addTranslation time element)
        |> Maybe.map transform
        |> Maybe.map (\a -> [ a ])
        |> Maybe.withDefault []


adjustTime : Element -> (Time -> a -> a) -> Time -> a -> a
adjustTime element function time a =
    if Flags.hasFlag Flags.targetFlag element.flags then
        a
    else
        function time a


addRotation : Time -> Element -> Maybe String
addRotation time element =
    element.rotation
        |> Maybe.map (adjustTime element rotateAtTime time)
        |> Maybe.map (rotationTransform <| Elements.rotationCenter element)


addTranslation : Time -> Element -> Maybe String
addTranslation time element =
    element.translation
        |> Maybe.map (adjustTime element translateAtTime time)
        |> Maybe.map translationTransform


translateAtTime : Time -> Translate -> Translate
translateAtTime time (Translate translate) =
    let
        targetTime : Time
        targetTime =
            TimeControl.add translate.delay translate.time

        timeRunning : Time
        timeRunning =
            TimeControl.difference time translate.delay

        fraction : Float
        fraction =
            TimeControl.divide timeRunning translate.time

        newRelativeTarget : Point
        newRelativeTarget =
            let
                target : Point
                target =
                    translate.relativeTarget
            in
                { x = target.x * fraction
                , y = target.y * fraction
                }
    in
        if TimeControl.isBeforeOrEqual time translate.delay then
            Translate { translate | relativeTarget = { x = 0, y = 0 } }
        else if TimeControl.isBeforeOrEqual time targetTime then
            Translate { translate | relativeTarget = newRelativeTarget }
        else
            (case translate.loop of
                NoLoop ->
                    Translate translate

                Restart ->
                    translateAtTime
                        (TimeControl.difference time translate.time)
                        (Translate translate)

                Return ->
                    if
                        TimeControl.isBeforeOrEqual
                            time
                            (TimeControl.twice targetTime)
                    then
                        translateAtTime
                            (translate.time
                                |> TimeControl.difference time
                                |> TimeControl.twice
                                |> TimeControl.difference time
                            )
                            (Translate translate)
                    else
                        translateAtTime
                            (TimeControl.difference time
                                (TimeControl.twice translate.time)
                            )
                            (Translate translate)
            )


rotateAtTime : Time -> Rotate -> Rotate
rotateAtTime time (Rotate rotate) =
    let
        targetTime : Time
        targetTime =
            TimeControl.add rotate.delay rotate.time

        fraction : Float
        fraction =
            TimeControl.divide
                (TimeControl.difference time rotate.delay)
                (TimeControl.difference rotate.time rotate.delay)
    in
        if TimeControl.isBeforeOrEqual time rotate.delay then
            Rotate { rotate | angle = Angle.zero }
        else if TimeControl.isBeforeOrEqual time targetTime then
            Rotate { rotate | angle = Angle.multiply fraction rotate.angle }
        else
            (case rotate.loop of
                NoLoop ->
                    Rotate rotate

                Restart ->
                    rotateAtTime
                        (TimeControl.difference time rotate.time)
                        (Rotate rotate)

                Return ->
                    if
                        TimeControl.isBeforeOrEqual
                            time
                            (TimeControl.twice targetTime)
                    then
                        rotateAtTime
                            (rotate.time
                                |> TimeControl.difference time
                                |> TimeControl.twice
                                |> TimeControl.difference time
                            )
                            (Rotate rotate)
                    else
                        rotateAtTime
                            (TimeControl.difference time
                                (TimeControl.twice rotate.time)
                            )
                            (Rotate rotate)
            )


rotationTransform : Point -> Rotate -> String
rotationTransform center (Rotate rotation) =
    " rotate("
        ++ (rotation.angle
                |> Animation.toDegrees
                |> toString
           )
        ++ " "
        ++ (toString2 <| center.x)
        ++ " "
        ++ (toString2 <| center.y)
        ++ ") "


translationTransform : Translate -> String
translationTransform translate =
    let
        target : Point
        target =
            Animation.getRelativeTarget translate
    in
        " translate("
            ++ (toString2 <| target.x)
            ++ " "
            ++ (toString2 <| target.y)
            ++ ") "


fillAttributes : WithFillProperties a -> List (Attribute b)
fillAttributes properties =
    [ fill "#000000"
    , fillOpacity
        (if properties.fill then
            "0.5"
         else
            "0"
        )
    ]


rectangleView : TwoPointsFillShape -> ViewParameters msg -> ShapeInfo -> Svg msg
rectangleView properties viewParams shape =
    rect
        ([ x <| toString2 <| Points.minX <| getPointsArrayFrom2P properties
         , y <| toString2 <| Points.minY <| getPointsArrayFrom2P properties
         , width <| toString2 <| shapeWidth <| getPointsArrayFrom2P properties
         , height <| toString2 <| shapeHeight <| getPointsArrayFrom2P properties
         , rx "0"
         , ry "0"
         ]
            ++ fillAttributes properties
            ++ commonAttributes properties viewParams shape
        )
        []


ellipseView : TwoPointsFillShape -> ViewParameters msg -> ShapeInfo -> Svg msg
ellipseView properties viewParams shape =
    ellipse
        ([ cx <| toString2 <| middleX <| getPointsArrayFrom2P properties
         , cy <| toString2 <| middleY <| getPointsArrayFrom2P properties
         , rx <| toString2 <| (shapeWidth <| getPointsArrayFrom2P properties) / 2
         , ry <| toString2 <| (shapeHeight <| getPointsArrayFrom2P properties) / 2
         ]
            ++ fillAttributes properties
            ++ commonAttributes properties viewParams shape
        )
        []


lineView : TwoPointsShape -> ViewParameters msg -> ShapeInfo -> Svg msg
lineView properties viewParams shape =
    line
        ([ x1 <| toString2 <| properties.p1.x
         , y1 <| toString2 <| properties.p1.y
         , x2 <| toString2 <| properties.p2.x
         , y2 <| toString2 <| properties.p2.y
         ]
            ++ commonAttributes properties viewParams shape
        )
        []


quadraticCurveView :
    ThreePointsShape
    -> ViewParameters msg
    -> ShapeInfo
    -> Svg msg
quadraticCurveView properties viewParams shape =
    let
        curveCoordinates =
            case properties.p3 of
                Nothing ->
                    "q 0 0"

                Just p3 ->
                    "q"
                        ++ toString2 (p3.x - properties.p1.x)
                        ++ " "
                        ++ toString2 (p3.y - properties.p1.y)
    in
        Svg.path
            ([ d
                ("M"
                    ++ toString2 properties.p1.x
                    ++ " "
                    ++ toString2 properties.p1.y
                    ++ " "
                    ++ curveCoordinates
                    ++ " "
                    ++ toString2 (properties.p2.x - properties.p1.x)
                    ++ " "
                    ++ toString2 (properties.p2.y - properties.p1.y)
                )
             , fill "transparent"
             ]
                ++ commonAttributes properties viewParams shape
            )
            []


polygonView : PointsFillShape -> ViewParameters msg -> ShapeInfo -> Svg msg
polygonView properties viewParams shape =
    if Array.length properties.points == 2 then
        lineView
            { p1 = Array.get 0 properties.points |> Maybe.withDefault bogusPoint
            , p2 = Array.get 1 properties.points |> Maybe.withDefault bogusPoint
            , strokeWidth = properties.strokeWidth
            , id = properties.id
            }
            viewParams
            shape
    else
        let
            onePoint : Point -> String -> String
            onePoint point s =
                s
                    ++ toString2 point.x
                    ++ ","
                    ++ toString2 point.y
                    ++ " "

            points : Array Point -> String
            points points =
                Array.foldr onePoint "" points
        in
            polygon
                ([ Svg.Attributes.points <| points properties.points ]
                    ++ fillAttributes properties
                    ++ commonAttributes properties viewParams shape
                )
                []


groupView : ViewParameters msg -> ( Group, Element ) -> Svg msg
groupView params ( group, element ) =
    Svg.g
        ([ class "shape-group" ] ++ addAnimation params.time element)
        (group
            |> Group.toList
            |> List.map
                (\shape ->
                    shapeView params
                        { shape = shape
                        , element = element
                        , isGrouped = True
                        }
                )
        )
