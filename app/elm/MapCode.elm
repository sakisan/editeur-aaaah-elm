module MapCode exposing (mapCode, elementsCode)

import Shapes.Animation as Animation exposing (..)
import Shapes.Element as Elements exposing (..)
import Shapes.Group as Group exposing (..)
import Shapes.Point as Points exposing (..)
import Shapes.Shape as Shapes exposing (..)
import Shapes.StrokeWidth as StrokeWidth
import TimeControl exposing (..)
import Utils exposing (..)


-- elm-lang packages

import Array exposing (Array)
import String


mapCode : ElementSet -> String
mapCode elements =
    "<C><G>"
        ++ groupsCode elements
        ++ "</G><F>"
        ++ (shapesCode elements)
        ++ "</F></C>"


elementsCode : ElementSet -> String
elementsCode elements =
    groupsCode elements ++ (shapesCode elements)


shapesCode : ElementSet -> String
shapesCode elements =
    Elements.getShapeInfos elements
        |> List.foldr appendShapeCode ""


groupsCode : ElementSet -> String
groupsCode elements =
    Elements.getGroupsTupled elements
        |> List.foldr appendGroupCode ""


appendGroupCode : ( Group, Element ) -> String -> String
appendGroupCode ( group, element ) string =
    let
        animation : Maybe String
        animation =
            animationCode element
    in
        string
            ++ "<G P=\""
            ++ (toString2 <| Group.minX group)
            ++ ","
            ++ (toString2 <| Group.minY group)
            ++ "\">"
            ++ (if isPresent animation then
                    groupCodeWithAnimation group element animation
                else
                    groupCodeNoAnimations group element
               )
            ++ "</G>"


groupCodeNoAnimations : Group -> Element -> String
groupCodeNoAnimations group element =
    Group.fold
        (\shape ->
            appendShapeCode
                { shape = shape
                , element = element
                , isGrouped = True
                }
        )
        ""
        group


groupCodeWithAnimation : Group -> Element -> Maybe String -> String
groupCodeWithAnimation group element animation =
    let
        shapeA : Shape
        shapeA =
            Group.getShapeA group

        shapeACode : String
        shapeACode =
            shapeCodeWithAnimation shapeA animation

        leftOver : Either Group Shape
        leftOver =
            Group.remove shapeA group
    in
        case leftOver of
            This group ->
                shapeACode
                    ++ groupCodeNoAnimations group element

            That shape ->
                appendShapeCode
                    { shape = shape
                    , element = element
                    , isGrouped = True
                    }
                    shapeACode


appendShapeCode : ShapeInfo -> String -> String
appendShapeCode shapeInfo string =
    string ++ shapeCode shapeInfo


animationAndClose : Maybe String -> String -> String
animationAndClose animation closeTag =
    animation
        |> Maybe.map (\string -> "\">" ++ string ++ closeTag)
        |> Maybe.withDefault "\"/>"


shapeCode : ShapeInfo -> String
shapeCode shapeInfo =
    let
        animation : Maybe String
        animation =
            if shapeInfo.isGrouped then
                Nothing
            else
                animationCode shapeInfo.element
    in
        shapeCodeWithAnimation shapeInfo.shape animation


shapeCodeWithAnimation : Shape -> Maybe String -> String
shapeCodeWithAnimation shape animation =
    let
        points : Array Point
        points =
            getPointsArray shape
    in
        case shape of
            Rectangle p ->
                "<R P=\""
                    ++ pStrokeWidth p
                    ++ ","
                    ++ pOriginXy points
                    ++ ","
                    ++ pDimensions points
                    ++ ","
                    ++ pFill p
                    ++ (animationAndClose animation "</R>")

            Ellipse p ->
                "<E P=\""
                    ++ pStrokeWidth p
                    ++ ","
                    ++ pOriginXy points
                    ++ ","
                    ++ pDimensions points
                    ++ ","
                    ++ pFill p
                    ++ (animationAndClose animation "</E>")

            Line p ->
                let
                    d =
                        pointDifference p.p2 p.p1
                in
                    "<L P=\""
                        ++ pStrokeWidth p
                        ++ ","
                        ++ (toString2 <| p.p1.x)
                        ++ ","
                        ++ (toString2 <| p.p1.y)
                        ++ ","
                        ++ (toString2 <| d.x)
                        ++ ","
                        ++ (toString2 <| d.y)
                        ++ (animationAndClose animation "</L>")

            QuadraticCurve p ->
                let
                    d =
                        pointDifference p.p2 p.p1

                    d2 =
                        pointDifference (Maybe.withDefault p.p1 p.p3) p.p1
                in
                    "<C P=\""
                        ++ pStrokeWidth p
                        ++ ","
                        ++ (toString2 <| p.p1.x)
                        ++ ","
                        ++ (toString2 <| p.p1.y)
                        ++ ","
                        ++ (toString2 <| d2.x)
                        ++ ","
                        ++ (toString2 <| d2.y)
                        ++ ","
                        ++ (toString2 <| d.x)
                        ++ ","
                        ++ (toString2 <| d.y)
                        ++ (animationAndClose animation "</C>")

            Polygon p ->
                "<P "
                    ++ "Z=\""
                    ++ pPolygonZ points
                    ++ "\" P=\""
                    ++ pStrokeWidth p
                    ++ ","
                    ++ (toString2 <| .x <| p1 points)
                    ++ ","
                    ++ (toString2 <| .y <| p1 points)
                    ++ ","
                    ++ pDimensions points
                    ++ ","
                    ++ pFill p
                    ++ (animationAndClose animation "</P>")


pStrokeWidth : WithCommonProperties a -> String
pStrokeWidth a =
    StrokeWidth.toString_ a.strokeWidth


pOriginXy : Array Point -> String
pOriginXy a =
    (toString2 <| Points.minX a)
        ++ ","
        ++ (toString2 <| Points.minY a)


pDimensions : Array Point -> String
pDimensions a =
    (toString2 <| shapeWidth a)
        ++ ","
        ++ (toString2 <| shapeHeight a)


pFill : WithFillProperties p -> String
pFill a =
    if a.fill then
        "1"
    else
        "0"


pPolygonZ : Array Point -> String
pPolygonZ points =
    let
        calcDifference : Int -> Point -> Point
        calcDifference i p =
            case Array.get 0 points of
                Nothing ->
                    { x = 0, y = 0 }

                Just p0 ->
                    pointDifference p p0

        appendCoordinates : Point -> String -> String
        appendCoordinates p s =
            s
                ++ (if String.length s == 0 then
                        ""
                    else
                        ";"
                   )
                ++ toString2 p.x
                ++ ","
                ++ toString2 p.y
    in
        points
            |> Array.indexedMap calcDifference
            |> Array.filter (\p -> not (p.x == 0 && p.y == 0))
            |> Array.foldl appendCoordinates ""


animationCode : Element -> Maybe String
animationCode element =
    let
        code : String
        code =
            ""
                ++ (Maybe.map rotationCode element.rotation
                        |> Maybe.withDefault ""
                   )
                ++ (Maybe.map (translationCode element) element.translation
                        |> Maybe.withDefault ""
                   )
    in
        Just code


translationCode : Element -> Translate -> String
translationCode element (Translate translation) =
    "<T P=\""
        ++ (delayCode translation.delay)
        ++ ","
        ++ (timeCode translation.time)
        ++ ","
        ++ (translationTargetCode
                (Elements.origin element)
                translation.relativeTarget
           )
        ++ ","
        ++ (loopCode translation.loop)
        ++ "\" />"


rotationCode : Rotate -> String
rotationCode (Rotate rotation) =
    "<R P=\""
        ++ (delayCode rotation.delay)
        ++ ","
        ++ (timeCode rotation.time)
        ++ ","
        ++ (toString0up <| Animation.toDegrees rotation.angle)
        ++ ","
        ++ (loopCode rotation.loop)
        ++ "\" />"


delayCode : Time -> String
delayCode (Time delay) =
    toString delay


timeCode : Time -> String
timeCode (Time time) =
    toString time


loopCode : Loop -> String
loopCode loop =
    case loop of
        NoLoop ->
            "0"

        Restart ->
            "1"

        Return ->
            "2"


translationTargetCode : Point -> Point -> String
translationTargetCode origin relativeTarget =
    let
        target : Point
        target =
            pointSum origin relativeTarget
    in
        toString target.x ++ "," ++ (toString target.y)
