module ParseCode exposing (parseCode, log)

import Array
import List.Extra as ListExtra
import Parser exposing (..)
import Shapes.Animation as Animation exposing (Rotate, Translate)
import Shapes.Element as Elements exposing (..)
import Shapes.Group as Group
import Shapes.ID as ID exposing (ID)
import Shapes.Point as Point exposing (Point)
import Shapes.Shape as Shape exposing (..)
import Shapes.StrokeWidth as StrokeWidth exposing (StrokeWidth)
import TimeControl
import Utils exposing (..)


parseCode : List ID -> String -> Result Error ElementSet
parseCode idList code =
    code
        |> removeWhiteSpace
        |> run (fullParser idList)


fullParser : List ID -> Parser ElementSet
fullParser idList =
    succeed
        (\groups shapes ->
            List.append groups shapes
                |> assignIDs idList
                |> Elements.fromList
        )
        |. ignoreUntil "<C>"
        |. ignoreUntil "<G>"
        |= repeat zeroOrMore groupParser
        |. maybeCloseTag
        |. ignoreUntil "<F>"
        |= repeat zeroOrMore shapeElementParser


log : Error -> String
log e =
    "=== === ===\n"
        ++ "row: "
        ++ (toString e.row)
        ++ " col: "
        ++ (toString e.col)
        ++ "\nsource: "
        ++ (toString e.source)
        ++ "\nproblem: "
        ++ (toString e.problem)
        ++ "\ncontext: "
        ++ (toString e.context)
        ++ "\n"
        ++ String.left e.col e.source
        ++ "\n"
        ++ String.dropLeft e.col e.source
        ++ "\n"
        ++ "\n"


bogusID : ID
bogusID =
    ID.fromString "bogus-import-id"


assignIDs : List ID -> List Element -> List Element
assignIDs idList elementList =
    ListExtra.zip idList elementList
        |> List.map
            (\( id, element ) ->
                element
                    |> Elements.setID id
            )


groupParser : Parser Element
groupParser =
    succeed
        (\elements ->
            let
                translation : Maybe Translate
                translation =
                    elements
                    |> List.head
                    |> Maybe.andThen .translation

                rotation : Maybe Rotate
                rotation =
                    elements
                    |> List.head
                    |> Maybe.andThen .rotation
            in
            elements
                |> List.map Elements.getAllShapesSingle
                |> List.concat
                |> List.indexedMap
                    (\i shape ->
                        Shape.setID (i |> toString |> ID.fromString) shape
                    )
                |> Group.fromList
                |> (\maybeGroup ->
                        case maybeGroup of
                            Just group ->
                                group

                            Nothing ->
                                Debug.crash
                                    "Group could not be parsed"
                   )
                |> Elements.fromGroup
                |> Elements.withTranslation translation
                |> Elements.withRotation rotation
        )
        |. symbol "<G"
        |. ignoreUntil ">"
        |= repeat zeroOrMore shapeElementParser
        |. ignoreUntil ">"
        |. maybeCloseTag


shapeElementParser : Parser Element
shapeElementParser =
    succeed
        (\( shape, ( translate, rotate ) ) ->
            Elements.fromShape shape
                |> Elements.withTranslation
                    (Maybe.map
                        (\translate ->
                            translate
                                |> Animation.getRelativeTarget
                                |> flip
                                    Point.pointDifference
                                    (Shape.origin shape)
                                |> flip Animation.setRelativeTarget translate
                        )
                        translate
                    )
                |> Elements.withRotation rotate
        )
        |= shapeParser


shapeParser : Parser ( Shape, ( Maybe Translate, Maybe Rotate ) )
shapeParser =
    succeed (,)
        |= oneOf
            [ rectangle
            , ellipse
            , line
            , quadraticCurve
            , polygon
            ]
        |. ignoreUntil ">"
        |= maybeAnimations
        |. maybeCloseTag


maybeAnimations : Parser ( Maybe Translate, Maybe Rotate )
maybeAnimations =
    -- <R P="0,5,58,0" /><T P="0,5,340,44,0" />
    oneOf
        [ succeed (\r t -> ( t, Just r ))
            |= rotationParser
            |= oneOf
                [ succeed Just
                    |= translationParser
                , succeed Nothing
                ]
        , succeed (\t r -> ( Just t, r ))
            |= translationParser
            |= oneOf
                [ succeed Just
                    |= rotationParser
                , succeed Nothing
                ]
        , succeed ( Nothing, Nothing )
        ]


rotationParser : Parser Rotate
rotationParser =
    succeed
        (\delay time angle loop ->
            Animation.getRotate
                { delay = TimeControl.timeFromFloat (toFloat delay)
                , time = TimeControl.timeFromFloat (toFloat time)
                , angle = Animation.fromDegrees (angle - 1)
                , loop = Animation.toLoop loop
                }
        )
        |. keyword "<R"
        |. ignoreUntil "\""
        |= int
        |. symbol ","
        |= int
        |. symbol ","
        |= float
        |. symbol ","
        |= int
        |. ignoreUntil "/>"


translationParser : Parser Translate
translationParser =
    succeed
        (\delay time target loop ->
            Animation.getTranslate
                { delay = TimeControl.timeFromFloat (toFloat delay)
                , time = TimeControl.timeFromFloat (toFloat time)
                , relativeTarget = target
                , loop = Animation.toLoop loop
                }
        )
        |. keyword "<T"
        |. ignoreUntil "\""
        |= int
        |. symbol ","
        |= int
        |. symbol ","
        |= point
        |. symbol ","
        |= int
        |. ignoreUntil "/>"


maybeCloseTag : Parser ()
maybeCloseTag =
    oneOf
        [ closeTagParser
        , succeed ()
        ]


closeTagParser : Parser ()
closeTagParser =
    succeed ()
        |. symbol "</"
        |. ignoreUntil ">"


pParser : String -> (a -> b) -> Parser a -> Parser b
pParser shapeLetter shapeFunction propertiesParser =
    -- P="..."
    succeed shapeFunction
        |. keyword shapeLetter
        |. ignoreUntil "\""
        |= propertiesParser
        |. symbol "\""


rectangle : Parser Shape
rectangle =
    pParser "<R" Rectangle twoPointsFillShape


ellipse : Parser Shape
ellipse =
    pParser "<E" Ellipse twoPointsFillShape


line : Parser Shape
line =
    pParser "<L" Line twoPointsShape


quadraticCurve : Parser Shape
quadraticCurve =
    pParser "<C" QuadraticCurve threePointsShape


polygon : Parser Shape
polygon =
    succeed
        (\z p ->
            Polygon
                { points = polygonPoints z p
                , fill = p.fill
                , strokeWidth = p.strokeWidth
                , id = bogusID
                }
        )
        |. keyword "<P"
        |. keyword "Z"
        |. ignoreUntil "\""
        |= zParser
        |. symbol "\""
        |. ignoreUntil "\""
        |= twoPointsFillShape
        |. symbol "\""


polygonPoints : List Point -> TwoPointsFillShape -> Array.Array Point
polygonPoints points properties =
    properties.p1
        :: (points
                |> List.map (\p -> Point.pointSum p properties.p1)
           )
        |> Array.fromList


twoPointsFillShape : Parser TwoPointsFillShape
twoPointsFillShape =
    -- <R P="x,xx,xx,xx,x">
    -- 3,209,206,255,111,1
    succeed
        (\width p1 p2 fill ->
            { strokeWidth = width
            , p1 = p1
            , p2 = Point.pointSum p1 p2
            , fill = fill
            , id = bogusID
            }
        )
        |= strokeWidth
        |. symbol ","
        |= point
        |. symbol ","
        |= point
        |. symbol ","
        |= fill


twoPointsShape : Parser TwoPointsShape
twoPointsShape =
    succeed
        (\width p1 p2 ->
            { strokeWidth = width
            , p1 = p1
            , p2 = Point.pointSum p1 p2
            , id = bogusID
            }
        )
        |= strokeWidth
        |. symbol ","
        |= point
        |. symbol ","
        |= point


threePointsShape : Parser ThreePointsShape
threePointsShape =
    succeed
        (\width p1 p2 p3 ->
            { strokeWidth = width
            , p1 = p1
            , p2 = Point.pointSum p1 p2
            , p3 = Point.pointSum p1 p3 |> Just
            , id = bogusID
            }
        )
        |= strokeWidth
        |. symbol ","
        |= point
        |. symbol ","
        |= point
        |. symbol ","
        |= point


zParser : Parser (List Point)
zParser =
    repeat zeroOrMore (point |. ignore oneOrMore ((==) ';'))


strokeWidth : Parser StrokeWidth
strokeWidth =
    succeed StrokeWidth.fromInt
        |= int


fill : Parser Bool
fill =
    succeed ((==) 1)
        |= int


point : Parser Point
point =
    succeed Point
        |= float
        |. symbol ","
        |= float


float : Parser Float
float =
    succeed
        (\negative f ->
            if negative then
                -f
            else
                f
        )
        |= minusSign
        |= Parser.float


minusSign : Parser Bool
minusSign =
    oneOf
        [ map (\_ -> True) (symbol "-")
        , succeed False
        ]
