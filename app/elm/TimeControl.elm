module TimeControl exposing (..)

import Time exposing (..)


type alias TimeControl =
    { currentTime : Time
    , play : Play
    , refreshRate : Time
    }


type Play
    = Pause
    | Play


type Time
    = Time Float


zero : Time
zero =
    Time 0


toCoreTime : Time -> Time.Time
toCoreTime (Time f) =
    f * Time.second


toTime : Time.Time -> Time
toTime time =
    Time.inSeconds time
        |> Time


timeToFloat : Time -> Float
timeToFloat (Time f) =
    f


timeFromFloat : Float -> Time
timeFromFloat =
    Time


start : Play
start =
    Play


pause : Play
pause =
    Pause


twice : Time -> Time
twice a =
    add a a


add : Time -> Time -> Time
add a b =
    map2 (+) a b


difference : Time -> Time -> Time
difference a b =
    map2 (-) a b


divide : Time -> Time -> Float
divide a b =
    mapO (/) a b


isBeforeOrEqual : Time -> Time -> Bool
isBeforeOrEqual a b =
    mapO (<=) a b


map2 : (Float -> Float -> Float) -> Time -> Time -> Time
map2 fun (Time a) (Time b) =
    fun a b
        |> Time


mapO : (Float -> Float -> a) -> Time -> Time -> a
mapO fun (Time a) (Time b) =
    fun a b
