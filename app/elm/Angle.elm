module Angle exposing (..)


type Angle
    = Angle Float


zero : Angle
zero =
    Angle 0


multiply : Float -> Angle -> Angle
multiply factor =
    map ((*) factor)


map : (Float -> Float) -> Angle -> Angle
map fun (Angle angle) =
    fun angle |> Angle


addAngle : Angle -> Angle -> Angle
addAngle (Angle a) (Angle b) =
    Angle (a + b)


angleFromFloat : Float -> Angle
angleFromFloat =
    Angle
