module Selection
    exposing
        ( Selection(..)
        , empty
        , isSelected
        , member
        , addOrRemoveShape
        , addShape
        , removeShape
        , toList
        , toElement
        , hasID
        , getIfSingleElement
        )

import Shapes.ID as ID exposing (..)
import Shapes.Element as Elements exposing (Element, ElementSet)
import Shapes.Shape as Shape exposing (Shape)
import Shapes.Group as Group exposing (Group)
import Utils exposing (..)


type Selection
    = Empty
    | SelectShape Shape
    | SelectGroup Group


empty : Selection
empty =
    Empty


isSelected : Shape -> Selection -> Bool
isSelected shape selection =
    case selection of
        Empty ->
            False

        SelectShape selectedShape ->
            Shape.equals shape selectedShape

        SelectGroup group ->
            Group.hasShape shape group


member : Shape -> Selection -> Bool
member shape selection =
    case selection of
        Empty ->
            False

        SelectShape myShape ->
            Shape.equals myShape shape

        SelectGroup group ->
            Group.member shape group


addOrRemoveShape : Shape -> Selection -> Selection
addOrRemoveShape shape selection =
    if member shape selection then
        removeShape shape selection
    else
        addShape shape selection


addShape : Shape -> Selection -> Selection
addShape shape selection =
    case selection of
        Empty ->
            SelectShape shape

        SelectShape firstShape ->
            case Group.fromTwoShapes firstShape shape of
                Just group ->
                    SelectGroup group

                Nothing ->
                    SelectShape shape

        SelectGroup group ->
            Group.insert shape group
                |> SelectGroup


removeShape : Shape -> Selection -> Selection
removeShape shape selection =
    if member shape selection then
        case selection of
            Empty ->
                Empty

            SelectShape shape ->
                Empty

            SelectGroup group ->
                case Group.remove shape group of
                    This group ->
                        SelectGroup group

                    That shape ->
                        SelectShape shape
    else
        selection


toList : Selection -> List Shape
toList selection =
    case selection of
        Empty ->
            []

        SelectShape shape ->
            [ shape ]

        SelectGroup group ->
            Group.toList group


toElement : Selection -> Maybe Element
toElement selection =
    case selection of
        Empty ->
            Nothing

        SelectShape shape ->
            Just <| Elements.fromShape shape

        SelectGroup group ->
            Just <| Elements.fromGroup group


hasID : ID -> Selection -> Bool
hasID id selection =
    selection
        |> toList
        |> List.any (\shape -> ID.equals (Shape.getShapeID shape) id)


getIfSingleElement : ElementSet -> Selection -> Maybe Element
getIfSingleElement elements selection =
    case selection of
        Empty ->
            Nothing

        SelectShape shape ->
            if Elements.isGrouped shape elements then
                Nothing
            else
                Elements.getCoreElementByShape shape elements

        SelectGroup group ->
            let
                firstElement : Maybe Element
                firstElement =
                    Elements.getCoreElementByShape
                        (Group.getShapeA group)
                        elements
            in
                case firstElement of
                    Nothing ->
                        Nothing

                    Just first ->
                        if selectionInOneGroup first group elements then
                            if Elements.hasFullGroup group first then
                                Just first
                            else
                                Nothing
                        else
                            Nothing


selectionInOneGroup : Element -> Group -> ElementSet -> Bool
selectionInOneGroup first group elements =
    let
        sameElement : Element -> Shape -> Bool
        sameElement element shape =
            case Elements.getCoreElementByShape shape elements of
                Nothing ->
                    False

                Just secondElement ->
                    secondElement
                        |> Elements.equals first
    in
        group
            |> Group.toList
            |> List.all (sameElement first)
