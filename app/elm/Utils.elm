module Utils exposing (..)

import Array exposing (Array)
import Json.Decode as Json exposing (field)
import String
import Task
import VirtualDom


-- UTILS - functions


applyIf : (a -> Bool) -> (a -> a) -> a -> a
applyIf predicate function a =
    if predicate a then
        function a
    else
        a



-- UTILS - strings


appendIf : Bool -> String -> String -> String
appendIf predicate appendix base =
    if predicate then
        base ++ appendix
    else
        base


removeWhiteSpace : String -> String
removeWhiteSpace =
    String.words >> String.join ""


emptyIsZero : String -> Result String Float
emptyIsZero string =
    String.toFloat
        (if string == "" then
            "0"
            else
            string
        )


-- UTILS - numbers


toString0up : Float -> String
toString0up float =
    float
        + 1
        |> round 0
        |> toString
        |> toStringPostProcessing


toString2 : Float -> String
toString2 float =
    round 2 float
        |> toString
        |> toStringPostProcessing


toStringPostProcessing : String -> String
toStringPostProcessing string =
    string
        |> String.map
            (\c ->
                if c == ',' then
                    '.'
                else
                    c
            )
        |> (\s ->
                -- toFloat sometimes gives numbers like 10.0699999 instead of 10.07
                let
                    index =
                        String.indexes "." s
                            |> List.map (\i -> (String.length s) - i)
                            |> List.reverse
                            |> List.head
                            |> Maybe.withDefault 0
                in
                    if index > 3 then
                        String.dropRight (index - 2) s
                    else
                        s
           )


round : Int -> Float -> Float
round decimals float =
    if decimals == 0 then
        Basics.round float
            |> toFloat
    else if decimals < 0 then
        (round (decimals + 1) (float / 10)) * 10
    else
        (round (decimals - 1) (float * 10)) / 10


distance1d : Float -> Float -> Float
distance1d a b =
    if a > b then
        a - b
    else
        b - a


tryToInt : String -> Int -> Int
tryToInt input fallback =
    if String.length input == 0 then
        0
    else
        String.toInt input
            |> Result.withDefault fallback


average : Float -> Float -> Float
average a b =
    (a + b) / 2


angle : { x : Float, y : Float } -> { x : Float, y : Float } -> Float
angle vector1 vector2 =
    let
        u1 =
            vector1.x

        u2 =
            vector1.y

        v1 =
            vector2.x

        v2 =
            vector2.y

        direction =
            if u1 * v2 - u2 * v1 > 0 then
                1
            else
                -1
    in
        acos
            ((u1 * v1 + u2 * v2)
                / (sqrt (u1 * u1 + u2 * u2)
                    * sqrt (v1 * v1 + v2 * v2)
                  )
            )
            * direction



-- UTILS - arrays


updateLast : Array a -> a -> Array a
updateLast array a =
    Array.set (Array.length array - 1) a array



-- UTILS - lists


prepend : List a -> List a -> List a
prepend list1 list2 =
    List.append list2 list1


maybePrepend : Maybe a -> List a -> List a
maybePrepend maybe list =
    case maybe of
        Nothing ->
            list

        Just a ->
            a :: list


replaceLast : List a -> a -> List a
replaceLast list item =
    list
        |> List.reverse
        |> List.drop 1
        |> (::) item
        |> List.reverse


mapIf : (a -> Bool) -> (a -> a) -> List a -> List a
mapIf predicate mapper =
    List.map (applyIf predicate mapper)



-- UTILS - Maybe


isPresent : Maybe a -> Bool
isPresent maybe =
    case maybe of
        Just a ->
            True

        _ ->
            False


appendMaybes : Maybe String -> Maybe String -> Maybe String
appendMaybes a b =
    if ((not << isPresent) a) && ((not << isPresent) b) then
        Nothing
    else
        Maybe.withDefault "" b
            |> String.append (Maybe.withDefault "" a)
            |> Just



-- UTIlS - Either


type Either a b
    = This a
    | That b



-- UTILS - Cmd


toCmd : msg -> Cmd msg
toCmd msg =
    Task.perform identity (Task.succeed msg)



-- UTILS - id


type alias RecordWithId a =
    { a | id : String }


removeById : List (RecordWithId a) -> String -> List (RecordWithId a)
removeById existing target =
    removeByAccessor existing .id target


removeByAccessor : List a -> (a -> String) -> String -> List a
removeByAccessor existing accessor target =
    List.filter (\a -> (accessor a) /= target) existing



-- UTILS MouseActions
-- from https://github.com/fredcy/elm-svg-mouse-offset/blob/25cd72b3e6edae3962c384ed562ad37ce469b302/Main.elm


type alias Position =
    { x : Int, y : Int }


offsetPosition : Json.Decoder Position
offsetPosition =
    Json.map2 Position (field "pageX" Json.int) (field "pageY" Json.int)


mouseEvent : String -> (Position -> msg) -> VirtualDom.Property msg
mouseEvent event messager =
    let
        options =
            { preventDefault = True, stopPropagation = True }
    in
        VirtualDom.onWithOptions event options (Json.map messager offsetPosition)


mouseMove : (Position -> msg) -> VirtualDom.Property msg
mouseMove =
    mouseEvent "mousemove"


mouseOut : (Position -> msg) -> VirtualDom.Property msg
mouseOut =
    mouseEvent "mouseout"


mouseOver : (Position -> msg) -> VirtualDom.Property msg
mouseOver =
    mouseEvent "mouseover"


mouseClick : (Position -> msg) -> VirtualDom.Property msg
mouseClick =
    mouseEvent "click"


mouseDown : (Position -> msg) -> VirtualDom.Property msg
mouseDown =
    mouseEvent "mousedown"


mouseUp : (Position -> msg) -> VirtualDom.Property msg
mouseUp =
    mouseEvent "mouseup"
