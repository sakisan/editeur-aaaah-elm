module Main exposing (..)

-- internal packages

import Background exposing (..)
import MapCode exposing (..)
import MapSvg exposing (..)
import ParseCode exposing (..)
import Selection as Selection exposing (..)
import Shapes.Animation as Animation exposing (..)
import Shapes.Element as Elements exposing (..)
import Shapes.Flags as Flags exposing (..)
import Shapes.ID as ID exposing (..)
import Shapes.Point exposing (..)
import Shapes.Shape as Shapes exposing (..)
import Shapes.StrokeWidth as StrokeWidth exposing (..)
import TimeControl as TimeControl exposing (..)
import Utils exposing (..)


-- elm-lang packages

import Array exposing (..)
import Dict exposing (Dict)
import Html as H exposing (Html, h1, button, div, program, span, text)
import Html.Attributes as HA
import Html.Events as HE
import Keyboard exposing (KeyCode)
import Random
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Task
import Time
import VirtualDom
import Window exposing (Size)


-- elm-community packages

import Parser as Parser
import Random.Char as RandomChar
import Random.String as RandomString


main : Program Never Model Msg
main =
    H.program
        { init = init <| initialModel
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    { elements : ElementSet
    , currentStyle : TypeAndAllProperties
    , mode : Mode
    , window : Size
    , offset : Point
    , timeControl : TimeControl
    }


type Mode
    = Normal
    | Drawing (Maybe Shape)
    | Deleting
    | Moving (Maybe MoveData)
    | Selection Selection
    | Translate Element (Maybe MoveData)
    | Rotate Element (Maybe MoveData)


type alias MoveData =
    { element : Element
    , startPoint : Point
    , currentPoint : Point
    }


type ClickTarget
    = DrawingBoardClick
    | ShapeClick Shape



-- INIT


init : Model -> ( Model, Cmd Msg )
init initialModel =
    ( initialModel
    , Task.perform (\s -> InitWindowSize s) Window.size
    )


initialModel : Model
initialModel =
    { elements = Elements.empty
    , currentStyle = initialStyle
    , mode = Normal
    , window = initSize
    , offset = initOffset
    , timeControl = initTime
    }


initialStyle : TypeAndAllProperties
initialStyle =
    { strokeWidth = StrokeWidth.fromInt 3
    , fill = True
    , shapeType = RectangleType
    , id = ID.fromString ""
    }


initSize : Size
initSize =
    { width = 900
    , height = 500
    }


initOffset : Point
initOffset =
    { x = 50
    , y = 50
    }


initTime : TimeControl
initTime =
    { currentTime = TimeControl.zero
    , play = Pause
    , refreshRate = Time.second / 21 |> TimeControl.toTime
    }



-- UPDATE


type Msg
    = NoOp
    | DrawShape Shape Point
    | MoveShape MoveData
    | StartDrawingShape Point
    | DoneDrawingShape Shape
    | UpdateDrawingShape Shape
    | DeleteShape Shape
    | ClearAll
    | SetCurrentFill Bool
    | SetCurrentWidthFromString String
    | SetCurrentShape ShapeType
    | SetCurrentWidth StrokeWidth
    | SetWidthOnShape StrokeWidth Shape
    | WindowSize Size
    | InitWindowSize Size
    | KeyDown KeyCode
    | Escape
    | SetMode Mode
    | MouseUp ClickTarget Position
    | StartMoving MoveData
    | StopMoving
    | StartTranslating Element MoveData
    | StopTranslating
    | TranslateElement MoveData
    | StartRotating Element MoveData
    | StopRotating
    | RotateElement MoveData
    | ShapeDown ShapeInfo Position
    | NextShapeId String
    | SelectionAddOrRemove Shape
    | GroupSelection
    | TimeTick Time.Time
    | TimePlay
    | TimePause
    | TimeStop
    | ImportCode
    | DoImportCode (List String)
    | SetX1OnShape Float Shape
    | SetY1OnShape Float Shape
    | SetX2OnShape Float Shape
    | SetY2OnShape Float Shape
    | SetTranslationDelay Time Element
    | SetTranslationTime Time Element
    | SetTranslationLoop Loop Element
    | SetRotationDelay Time Element
    | SetRotationTime Time Element
    | SetRotationLoop Loop Element


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        { elements, currentStyle, timeControl } =
            model
    in
        case msg of
            NoOp ->
                ( model, Cmd.none )

            DrawShape shape point ->
                if Elements.hasShape shape model.elements then
                    -- mouse movements can be triggered too soon after
                    -- a DoneDrawingShape, then the parameters are out of sync
                    ( model, Cmd.none )
                else
                    ( { model
                        | mode =
                            Drawing <|
                                Just <|
                                    updateShapeLastPoint point shape
                      }
                    , Cmd.none
                    )

            MoveShape newMoveData ->
                ( { model
                    | mode =
                        -- sanity check
                        case model.mode of
                            Moving maybeMove ->
                                case maybeMove of
                                    Just move ->
                                        Moving <| Just newMoveData

                                    Nothing ->
                                        model.mode

                            _ ->
                                model.mode
                  }
                , Cmd.none
                )

            StartDrawingShape point ->
                ( { model
                    | mode =
                        Drawing <| Just <| newShape model.currentStyle point
                  }
                , Cmd.none
                )

            DoneDrawingShape shape ->
                let
                    reviewedShape : Shape
                    reviewedShape =
                        Shapes.review shape
                in
                    ( { model
                        | elements =
                            Elements.insertShape
                                reviewedShape
                                model.elements
                        , mode = Drawing Nothing
                      }
                    , generateRandomID
                    )

            UpdateDrawingShape shape ->
                ( { model | mode = Drawing <| Just shape }, Cmd.none )

            DeleteShape shape ->
                ( { model
                    | elements = Elements.removeShape shape model.elements
                  }
                , Cmd.none
                )

            ClearAll ->
                ( { model
                    | elements = Elements.empty
                    , mode = Normal
                  }
                , Cmd.none
                )

            SetCurrentFill f ->
                ( { model
                    | currentStyle = { currentStyle | fill = f }
                    , elements = updateFillOnSelection f model.mode model.elements
                  }
                , Cmd.none
                )

            SetCurrentWidthFromString input ->
                ( updateCurrentWidth
                    (\w -> StrokeWidth.fromString input w)
                    model
                , Cmd.none
                )

            SetWidthOnShape input target ->
                ( { model
                    | elements =
                        updateTargetShape
                            (Shapes.setStrokeWidth input)
                            target
                            elements
                  }
                , Cmd.none
                )

            SetCurrentShape shapeType ->
                ( { model
                    | currentStyle =
                        { currentStyle | shapeType = shapeType }
                    , mode = Drawing Nothing
                  }
                , Cmd.none
                )

            SetCurrentWidth i ->
                ( updateCurrentWidth (\w -> i) model
                , Cmd.none
                )

            WindowSize size ->
                ( { model | window = size }, Cmd.none )

            InitWindowSize size ->
                ( { model | window = size }, generateRandomID )

            KeyDown keyCode ->
                case keyCode of
                    -- escape
                    27 ->
                        ( model, toCmd Escape )

                    -- enter
                    13 ->
                        let
                            cmd =
                                case model.mode of
                                    Drawing maybe ->
                                        case maybe of
                                            Just shape ->
                                                toCmd <|
                                                    DoneDrawingShape shape

                                            _ ->
                                                Cmd.none

                                    _ ->
                                        Cmd.none
                        in
                            ( model, cmd )

                    _ ->
                        ( model, Cmd.none )

            Escape ->
                ( { model | mode = Normal }
                , Cmd.none
                )

            SetMode mode ->
                ( { model | mode = mode }, Cmd.none )

            MouseUp target position ->
                ( model, toCmd <| mouseClickAction target model.mode position )

            ShapeDown shape position ->
                handleShapeDown shape position model

            StartMoving moveData ->
                ( { model | mode = Moving <| Just moveData }, Cmd.none )

            StopMoving ->
                case model.mode of
                    Moving maybeMove ->
                        ( { model
                            | mode = Moving Nothing
                            , elements = doMove maybeMove model.elements
                          }
                        , Cmd.none
                        )

                    _ ->
                        ( model, Cmd.none )

            StartTranslating element moveData ->
                ( { model | mode = Translate element (Just moveData) }
                , Cmd.none
                )

            StopTranslating ->
                case model.mode of
                    Translate element moveData ->
                        let
                            newElements : ElementSet
                            newElements =
                                doTranslate element moveData model.elements

                            newElement : Element
                            newElement =
                                -- get the element with the updated translation
                                Elements.getElement element newElements
                                    |> Maybe.withDefault element
                        in
                            ( { model
                                | mode = Translate newElement Nothing
                                , elements = newElements
                              }
                            , Cmd.none
                            )

                    _ ->
                        ( model, Cmd.none )

            TranslateElement moveData ->
                case model.mode of
                    Translate element maybeMove ->
                        case maybeMove of
                            Just move ->
                                ( { model
                                    | mode = Translate element (Just moveData)
                                  }
                                , Cmd.none
                                )

                            _ ->
                                ( model, Cmd.none )

                    _ ->
                        ( model, Cmd.none )

            StartRotating element moveData ->
                ( { model | mode = Rotate element (Just moveData) }
                , Cmd.none
                )

            StopRotating ->
                case model.mode of
                    Rotate element moveData ->
                        let
                            newElement : Element
                            newElement =
                                maybeDoRotation moveData element

                            newElements : ElementSet
                            newElements =
                                model.elements
                                    |> Elements.removeElement element
                                    |> Elements.insert newElement
                        in
                            ( { model
                                | mode = Rotate newElement Nothing
                                , elements = newElements
                              }
                            , Cmd.none
                            )

                    _ ->
                        ( model, Cmd.none )

            RotateElement moveData ->
                case model.mode of
                    Rotate element maybeMove ->
                        case maybeMove of
                            Just move ->
                                ( { model
                                    | mode = Rotate element (Just moveData)
                                  }
                                , Cmd.none
                                )

                            _ ->
                                ( model, Cmd.none )

                    _ ->
                        ( model, Cmd.none )

            NextShapeId randomString ->
                let
                    id : ID
                    id =
                        ID.fromString randomString
                in
                    ( { model
                        | currentStyle = { currentStyle | id = id }
                        , mode =
                            case model.mode of
                                Drawing maybe ->
                                    case maybe of
                                        Just shape ->
                                            Drawing <|
                                                Just <|
                                                    Shapes.setID id shape

                                        _ ->
                                            model.mode

                                _ ->
                                    model.mode
                      }
                    , Cmd.none
                    )

            SelectionAddOrRemove shape ->
                case model.mode of
                    Selection selection ->
                        ( { model
                            | mode =
                                selectionAddOrRemove
                                    shape
                                    model.elements
                                    selection
                          }
                        , Cmd.none
                        )

                    _ ->
                        ( model, Cmd.none )

            GroupSelection ->
                case model.mode of
                    Selection selection ->
                        ( { model
                            | mode = Selection Selection.empty
                            , elements = groupSelection selection model.elements
                          }
                        , Cmd.none
                        )

                    _ ->
                        ( { model | mode = Selection Selection.empty }
                        , Cmd.none
                        )

            TimeTick time ->
                ( { model | timeControl = tick model.timeControl }
                , Cmd.none
                )

            TimePlay ->
                ( { model
                    | timeControl =
                        { timeControl | play = TimeControl.start }
                  }
                , Cmd.none
                )

            TimePause ->
                ( { model
                    | timeControl =
                        { timeControl | play = TimeControl.pause }
                  }
                , Cmd.none
                )

            TimeStop ->
                ( { model
                    | timeControl =
                        { timeControl
                            | play = TimeControl.pause
                            , currentTime = TimeControl.zero
                        }
                  }
                , Cmd.none
                )

            ImportCode ->
                ( model, generateRandomIDList 1000 DoImportCode )

            DoImportCode idList ->
                let
                    parseResult : Result Parser.Error ElementSet
                    parseResult =
                        parseCode
                            (idList |> List.map ID.fromString)
                            (mapCode elements)
                in
                    ( { model
                        | elements =
                            (case parseResult of
                                Ok elementSet ->
                                    elementSet

                                Err e ->
                                    elements
                                        |> Debug.log (ParseCode.log e)
                            )
                      }
                    , Cmd.none
                    )

            SetX1OnShape input target ->
                ( { model
                    | elements =
                        updateTargetShape
                            (Shapes.setX1 input)
                            target
                            elements
                  }
                , Cmd.none
                )

            SetY1OnShape input target ->
                ( { model
                    | elements =
                        updateTargetShape
                            (Shapes.setY1 input)
                            target
                            elements
                  }
                , Cmd.none
                )

            SetX2OnShape input target ->
                ( { model
                    | elements =
                        updateTargetShape
                            (Shapes.setX2 input)
                            target
                            elements
                  }
                , Cmd.none
                )

            SetY2OnShape input target ->
                ( { model
                    | elements =
                        updateTargetShape
                            (Shapes.setY2 input)
                            target
                            elements
                  }
                , Cmd.none
                )

            SetTranslationDelay input target ->
                ( { model
                    | elements =
                        updateTargetTranslation
                            (\(Animation.Translate translate) ->
                                { translate | delay = input }
                                    |> Animation.Translate
                            )
                            target
                            elements
                  }
                , Cmd.none
                )

            SetTranslationTime input target ->
                ( { model
                    | elements =
                        updateTargetTranslation
                            (\(Animation.Translate translate) ->
                                { translate | time = input }
                                    |> Animation.Translate
                            )
                            target
                            elements
                  }
                , Cmd.none
                )

            SetTranslationLoop loop target ->
                ( { model
                    | elements =
                        updateTargetTranslation
                            (\(Animation.Translate translate) ->
                                { translate | loop = loop }
                                    |> Animation.Translate
                            )
                            target
                            elements
                  }
                , Cmd.none
                )

            SetRotationDelay input target ->
                ( { model
                    | elements =
                        updateTargetRotation
                            (\(Animation.Rotate translate) ->
                                { translate | delay = input }
                                    |> Animation.Rotate
                            )
                            target
                            elements
                  }
                , Cmd.none
                )

            SetRotationTime input target ->
                ( { model
                    | elements =
                        updateTargetRotation
                            (\(Animation.Rotate translate) ->
                                { translate | time = input }
                                    |> Animation.Rotate
                            )
                            target
                            elements
                  }
                , Cmd.none
                )

            SetRotationLoop loop target ->
                ( { model
                    | elements =
                        updateTargetRotation
                            (\(Animation.Rotate translate) ->
                                { translate | loop = loop }
                                    |> Animation.Rotate
                            )
                            target
                            elements
                  }
                , Cmd.none
                )


updateTargetRotation :
    (Animation.Rotate -> Animation.Rotate)
    -> Element
    -> ElementSet
    -> ElementSet
updateTargetRotation function target elements =
    updateTargetElement
        (\element ->
            { element
                | rotation =
                    Maybe.map function element.rotation
            }
        )
        target
        elements


updateTargetTranslation :
    (Animation.Translate -> Animation.Translate)
    -> Element
    -> ElementSet
    -> ElementSet
updateTargetTranslation function target elements =
    updateTargetElement
        (\element ->
            { element
                | translation =
                    Maybe.map function element.translation
            }
        )
        target
        elements


updateTargetElement :
    (Element -> Element)
    -> Element
    -> ElementSet
    -> ElementSet
updateTargetElement function target =
    Elements.mapElements
        (\element ->
            if Elements.equals element target then
                function element
            else
                element
        )


updateTargetShape : (Shape -> Shape) -> Shape -> ElementSet -> ElementSet
updateTargetShape function target elements =
    Elements.mapShapes
        (\shape ->
            if Shapes.equals shape target then
                function shape
            else
                shape
        )
        elements


tick : TimeControl -> TimeControl
tick timeControl =
    case timeControl.play of
        Pause ->
            timeControl

        Play ->
            { timeControl
                | currentTime =
                    TimeControl.add
                        timeControl.refreshRate
                        timeControl.currentTime
            }


handleShapeDown : ShapeInfo -> Position -> Model -> ( Model, Cmd Msg )
handleShapeDown shapeInfo position model =
    let
        newMoveData : Element -> MoveData
        newMoveData element =
            { element = element
            , startPoint = toPoint position
            , currentPoint = toPoint position
            }

        coreElementDown : Element
        coreElementDown =
            Elements.getCoreElementByShape shapeInfo.shape model.elements
                |> Maybe.withDefault (Elements.fromShape shapeInfo.shape)

        isAction : Bool
        isAction =
            Flags.hasFlag Flags.actionFlag shapeInfo.element.flags
    in
        case model.mode of
            Moving maybeMove ->
                ( model
                , toCmd <| StartMoving <| newMoveData coreElementDown
                )

            Translate element maybeMove ->
                if isAction then
                    ( model
                    , newMoveData shapeInfo.element
                        |> StartTranslating element
                        |> toCmd
                    )
                else
                    ( model, Cmd.none )

            Rotate element maybe ->
                if isAction then
                    ( model
                    , newMoveData element
                        |> StartRotating element
                        |> toCmd
                    )
                else
                    ( model, Cmd.none )

            _ ->
                ( model, Cmd.none )


updateFillOnSelection : Bool -> Mode -> ElementSet -> ElementSet
updateFillOnSelection fill mode elements =
    case mode of
        Selection selection ->
            Elements.mapShapes
                (\shape ->
                    if Selection.hasID (Shapes.getShapeID shape) selection then
                        Shapes.updateFill fill shape
                    else
                        shape
                )
                elements

        _ ->
            elements


groupSelection : Selection -> ElementSet -> ElementSet
groupSelection selection elements =
    case Selection.toElement selection of
        Just element ->
            elements
                |> Elements.removeAll (Selection.toList selection)
                |> Elements.insert element

        Nothing ->
            elements


selectionAddOrRemove : Shape -> ElementSet -> Selection -> Mode
selectionAddOrRemove shape elements selection =
    case Elements.getCoreElementByShape shape elements of
        Just element ->
            Selection.addOrRemoveShape shape selection
                |> Selection

        Nothing ->
            Selection selection


generateRandomID : Cmd Msg
generateRandomID =
    RandomString.string 12 RandomChar.english
        |> Random.generate NextShapeId


generateRandomIDList : Int -> (List String -> Msg) -> Cmd Msg
generateRandomIDList size msg =
    RandomString.string 12 RandomChar.english
        |> Random.list size
        |> Random.generate msg


doMove : Maybe MoveData -> ElementSet -> ElementSet
doMove move elements =
    case move of
        Nothing ->
            elements

        Just data ->
            case Elements.getElement data.element elements of
                Just element ->
                    elements
                        |> Elements.removeElement data.element
                        |> Elements.insert (data.element |> moveOffset data)

                Nothing ->
                    elements


doTranslate : Element -> Maybe MoveData -> ElementSet -> ElementSet
doTranslate element maybeMove elements =
    case maybeMove of
        Just move ->
            elements
                |> Elements.mapElements
                    (\e ->
                        if Elements.equals e element then
                            Elements.adjustTranslation
                                (getMoveOffset move)
                                element
                        else
                            e
                    )

        _ ->
            elements


maybeDoRotation : Maybe MoveData -> Element -> Element
maybeDoRotation maybe element =
    case maybe of
        Just move ->
            doRotation move element

        Nothing ->
            element


doRotation : MoveData -> Element -> Element
doRotation moveData element =
    { element
        | rotation =
            Animation.toRotation
                moveData.startPoint
                moveData.currentPoint
                (Elements.rotationCenter element)
                |> Animation.addAngle element.rotation
                |> Just
    }


updateCurrentWidth : (StrokeWidth -> StrokeWidth) -> Model -> Model
updateCurrentWidth update model =
    let
        currentStyle =
            model.currentStyle

        strokeWidth =
            currentStyle.strokeWidth

        newWidth =
            update strokeWidth
    in
        { model
            | currentStyle =
                { currentStyle | strokeWidth = newWidth }
        }



-- mouse actions


subscribeMouseMove : Model -> List (VirtualDom.Property Msg)
subscribeMouseMove model =
    let
        subscribe : Bool
        subscribe =
            case model.mode of
                Drawing maybe ->
                    isPresent maybe

                Moving maybe ->
                    isPresent maybe

                Translate element maybe ->
                    isPresent maybe

                Rotate element maybe ->
                    isPresent maybe

                _ ->
                    False
    in
        if subscribe then
            [ mouseMove <| mouseMoveAction model
            ]
        else
            []


mouseMoveAction : Model -> Position -> Msg
mouseMoveAction model position =
    let
        point : Point
        point =
            toPoint position
    in
        -- make sure to add a double check in the branch of the update method
        -- as well, as mousemoves can arrive a bit delayed after a mouseup
        case model.mode of
            Drawing maybe ->
                case maybe of
                    Just shape ->
                        DrawShape shape point

                    _ ->
                        NoOp

            Moving maybeMove ->
                case maybeMove of
                    Just moveData ->
                        MoveShape
                            { moveData
                                | currentPoint = point
                            }

                    _ ->
                        NoOp

            Translate element maybeMove ->
                case maybeMove of
                    Just moveData ->
                        TranslateElement
                            { moveData
                                | currentPoint = point
                            }

                    _ ->
                        NoOp

            Rotate element maybeMove ->
                case maybeMove of
                    Just moveData ->
                        RotateElement
                            { moveData
                                | currentPoint = point
                            }

                    _ ->
                        NoOp

            _ ->
                NoOp


mouseMoveIsAction : Model -> Bool
mouseMoveIsAction model =
    case mouseMoveAction model { x = 0, y = 0 } of
        NoOp ->
            False

        _ ->
            True


mouseClickAction : ClickTarget -> Mode -> Position -> Msg
mouseClickAction target mode position =
    let
        point =
            toPoint position
    in
        case mode of
            Drawing maybe ->
                case maybe of
                    Nothing ->
                        StartDrawingShape point

                    Just shape ->
                        if pointLimitReached shape then
                            DoneDrawingShape shape
                        else
                            UpdateDrawingShape <|
                                insertPointToShape point shape

            Deleting ->
                case target of
                    DrawingBoardClick ->
                        NoOp

                    ShapeClick shape ->
                        DeleteShape shape

            Moving maybe ->
                case maybe of
                    Just data ->
                        StopMoving

                    Nothing ->
                        NoOp

            Normal ->
                NoOp

            Selection selection ->
                case target of
                    DrawingBoardClick ->
                        NoOp

                    ShapeClick shape ->
                        SelectionAddOrRemove shape

            Translate element maybe ->
                case maybe of
                    Just data ->
                        StopTranslating

                    Nothing ->
                        NoOp

            Rotate element maybe ->
                case maybe of
                    Just data ->
                        StopRotating

                    Nothing ->
                        NoOp



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        ([ Window.resizes WindowSize
         , Keyboard.downs KeyDown
         ]
            ++ (case model.timeControl.play of
                    Play ->
                        [ Time.every
                            (model.timeControl.refreshRate
                                |> TimeControl.toCoreTime
                            )
                            TimeTick
                        ]

                    Pause ->
                        []
               )
        )



-- VIEW


view : Model -> Html Msg
view model =
    div [ class (modeName model.mode) ]
        [ drawingBoard model
        , statusLine model
        , controlPanel model
        , mapCodeView model
        , focusCodeView model
        ]


drawingBoard : Model -> Html Msg
drawingBoard model =
    let
        normalSize : Size
        normalSize =
            { height = initSize.height
            , width = model.window.width
            }

        expandedSize : Size
        expandedSize =
            { width = model.window.width
            , height = model.window.height - 100
            }

        fullSize : Size
        fullSize =
            { width = model.window.width
            , height = model.window.height
            }

        boardSize : Size
        boardSize =
            if mouseMoveIsAction model then
                fullSize
            else
                normalSize
    in
        Svg.svg
            ([ id "drawingBoard"
             , width <| toString boardSize.width
             , height <| toString boardSize.height
             , viewBox <|
                "0 0 "
                    ++ toString boardSize.width
                    ++ " "
                    ++ toString boardSize.height
             , mouseUp <| MouseUp DrawingBoardClick
             ]
                ++ subscribeMouseMove model
            )
            (backgroundImage model.offset svgMessages
                ++ (elementsToSvg model)
            )


elementsToSvg : Model -> List (Svg Msg)
elementsToSvg model =
    mapSvg
        { messages = svgMessages
        , class = "shape"
        , classFor = classFunction model
        , time = model.timeControl.currentTime
        }
        (model.elements
            |> modeAdjustments model.mode
            |> withOrder drawOrder
        )


classFunction : Model -> ShapeInfo -> String
classFunction model shapeInfo =
    let
        classByFlag : List ( String, Flag ) -> Element -> String
        classByFlag mapping element =
            Dict.foldl
                (\className flag classBuilder ->
                    if Flags.hasFlag flag element.flags then
                        className ++ " " ++ classBuilder
                    else
                        classBuilder
                )
                ""
                (Dict.fromList mapping)
    in
        case model.mode of
            Selection selection ->
                if Selection.isSelected shapeInfo.shape selection then
                    "selected "
                else
                    ""

            Translate element move ->
                classByFlag
                    [ ( "target", Flags.targetFlag )
                    , ( "translating", Flags.subjectFlag )
                    , ( "action", Flags.actionFlag )
                    ]
                    shapeInfo.element

            Rotate element move ->
                classByFlag
                    [ ( "target", Flags.targetFlag )
                    , ( "rotating", Flags.subjectFlag )
                    , ( "handle", Flags.handleFlag )
                    , ( "action", Flags.actionFlag )
                    ]
                    shapeInfo.element

            _ ->
                ""


drawOrder : Element -> Element -> Order
drawOrder a b =
    let
        flagOrder : List ( Int, Flag )
        flagOrder =
            -- increasing draw "height"
            [ Flags.subjectFlag
            , Flags.targetFlag
            , Flags.actionFlag
            , Flags.handleFlag
            ]
                |> Array.fromList
                |> Array.toIndexedList

        precedence : Flags -> Int
        precedence flags =
            List.foldl
                (\( i, flag ) n ->
                    if Flags.hasFlag flag flags then
                        n + (2 ^ i)
                    else
                        n
                )
                0
                flagOrder
    in
        case Basics.compare (precedence a.flags) (precedence b.flags) of
            EQ ->
                Elements.elementsComparer a b

            LT ->
                LT

            GT ->
                GT


svgMessages : ViewMessages Msg
svgMessages =
    { shapeDown = (\shapeInfo -> ShapeDown shapeInfo)
    , shapeUp = (\shapeInfo -> MouseUp <| ShapeClick shapeInfo.shape)
    }


modeAdjustmentsCore : Mode -> ElementSet -> ElementSet
modeAdjustmentsCore mode elements =
    case mode of
        Drawing maybe ->
            case maybe of
                Just shape ->
                    Elements.insertShape shape elements

                Nothing ->
                    elements

        Translate element maybeMove ->
            doTranslate element maybeMove elements

        Rotate element maybeMove ->
            -- this if prevents the focus code
            -- from showing up when it shouldn't
            if isPresent maybeMove then
                elements
                    |> Elements.removeElement element
                    |> Elements.insert (maybeDoRotation maybeMove element)
            else
                elements

        Moving maybe ->
            doMove maybe elements

        _ ->
            elements


modeAdjustments : Mode -> ElementSet -> ElementSet
modeAdjustments mode elements =
    let
        coreElements : ElementSet
        coreElements =
            modeAdjustmentsCore mode elements
    in
        case mode of
            Translate element maybeMove ->
                let
                    subject : Element
                    subject =
                        element
                            |> Elements.flagAs Flags.subjectFlag
                            |> Elements.withNoAnimations

                    target : Element
                    target =
                        Elements.flagAs Flags.targetFlag element

                    endTarget : Element
                    endTarget =
                        case maybeMove of
                            Nothing ->
                                target

                            Just moveData ->
                                moveOffset moveData target

                    action : Element
                    action =
                        endTarget
                            |> Elements.flagAs Flags.actionFlag
                            |> Elements.withRotation Nothing
                in
                    doTranslate element maybeMove coreElements
                        |> Elements.insert action
                        |> Elements.insert subject

            Rotate element maybeMove ->
                let
                    subject : Element
                    subject =
                        element
                            |> Elements.flagAs Flags.subjectFlag
                            |> Elements.withNoAnimations

                    target : Element
                    target =
                        Elements.flagAs Flags.targetFlag element

                    endTarget : Element
                    endTarget =
                        maybeDoRotation maybeMove target

                    actionElement : Element
                    actionElement =
                        endTarget
                            |> Elements.flagAs Flags.actionFlag
                            |> Elements.withTranslation Nothing
                in
                    coreElements
                        |> Elements.insert actionElement
                        |> Elements.insert (centerHandle element)
                        |> Elements.removeElement element
                        |> Elements.insert
                            (maybeDoRotation maybeMove element)
                        |> Elements.insert subject

            _ ->
                coreElements


centerHandle : Element -> Element
centerHandle element =
    Elements.rotationCenter element
        |> Shapes.circle 5
            { id = ID.fromString "center"
            , fill = True
            , strokeWidth = StrokeWidth.fromInt 2
            }
        |> Elements.fromShape
        |> Elements.flagAs Flags.handleFlag


addFocusElements : Model -> ElementSet -> ElementSet
addFocusElements model elements =
    case model.mode of
        Translate element maybeMove ->
            if isPresent maybeMove then
                Elements.insert element elements
            else
                elements

        Rotate element maybeMove ->
            if isPresent maybeMove then
                Elements.insert element elements
            else
                elements

        Moving maybe ->
            case maybe of
                Just move ->
                    Elements.insert move.element elements

                Nothing ->
                    elements

        _ ->
            elements


moveOffset : MoveData -> Element -> Element
moveOffset move =
    getMoveOffset move
        |> moveShape
        |> Elements.mapShapesSingle


getMoveOffset : MoveData -> Point
getMoveOffset data =
    pointDifference data.currentPoint data.startPoint


shapeTypeName : ShapeType -> String
shapeTypeName shapeType =
    case shapeType of
        RectangleType ->
            "Rectangle"

        EllipseType ->
            "Ellipse"

        LineType ->
            "Ligne"

        QuadraticCurveType ->
            "Courbe"

        PolygonType ->
            "Polygone"


modeName : Mode -> String
modeName mode =
    case mode of
        Normal ->
            "normal"

        Drawing shape ->
            "drawing"

        Deleting ->
            "deleting"

        Moving maybeMove ->
            "moving"

        Selection selection ->
            "selection"

        Translate element move ->
            "translate"

        Rotate element maybe ->
            "rotate"


statusLine : Model -> Html Msg
statusLine model =
    let
        modeText =
            case model.mode of
                Normal ->
                    "éditeur"

                Drawing shape ->
                    "dessin - "
                        ++ (shapeTypeName model.currentStyle.shapeType)

                Deleting ->
                    "suppression"

                Moving maybeMove ->
                    "déplaçage"

                Selection selection ->
                    "selection"

                Translate element move ->
                    "translation"

                Rotate element maybe ->
                    "rotation"
    in
        H.p [ class ("statusline " ++ (modeName model.mode)) ]
            [ H.text ("Mode " ++ modeText) ]


controlPanel : Model -> Html Msg
controlPanel model =
    H.div []
        ([ span [ class "btn-group" ]
            [ shapeSelect "Rectangle" model RectangleType
            , shapeSelect "Ellipse" model EllipseType
            , shapeSelect "Ligne" model LineType
            , shapeSelect "Courbe" model QuadraticCurveType
            , shapeSelect "Polygone" model PolygonType
            ]
         , span [ class "btn-group modeTools" ]
            [ btn "deleting" "Gomme" (SetMode Deleting)
            , btn "moving" "Déplaceur" (SetMode <| Moving Nothing)
            , btn "selection" "Selection" (SetMode <| modeToSelection model)
            , btn "group" "Grouper" GroupSelection
            ]
         , btn "danger" "Effacer tout" ClearAll
         , btn "default" "Echap" Escape
         , span []
            [ H.text <|
                "Formes: "
                    ++ (model.elements
                            |> modeAdjustmentsCore model.mode
                            |> Elements.count
                            |> toString
                       )
            ]
         , div []
            [ H.label
                [ HA.for "strokeWidth" ]
                [ H.text "Épaisseur" ]
            , H.input
                [ type_ "text"
                , id "strokeWidth"
                , HE.onInput SetCurrentWidthFromString
                , model.currentStyle.strokeWidth
                    |> StrokeWidth.toStringRaw
                    |> HA.value
                ]
                []
            , span [ class "btn-group" ]
                (let
                    strokeMessage : Int -> Msg
                    strokeMessage i =
                        SetCurrentWidth <|
                            StrokeWidth.addFromInt
                                i
                                model.currentStyle.strokeWidth
                 in
                    [ btn "secondary" "+1" (strokeMessage 1)
                    , btn "secondary" "-1" (strokeMessage -1)
                    , btn "secondary" "+10" (strokeMessage 10)
                    , btn "secondary" "-10" (strokeMessage -10)
                    ]
                )
            , checkBox "Rempli"
                model.currentStyle.fill
                SetCurrentFill
            , span [ class "btn-group" ]
                [ btn "default" "play" TimePlay
                , btn "default" "pause" TimePause
                , btn "default" "stop" TimeStop
                , H.text <| timeText model.timeControl.currentTime
                ]
            ]
         , btn "default" "Import" ImportCode
         ]
            ++ (animationButtons model.mode model.elements)
            ++ (editShapeForms model model.elements)
            ++ (editAnimationForms model model.elements)
        )


timeText : Time -> String
timeText time =
    time
        |> TimeControl.timeToFloat
        |> toString2


singleElementAddition :
    (Element -> List (Html Msg))
    -> Mode
    -> ElementSet
    -> List (Html Msg)
singleElementAddition addition mode elements =
    case mode of
        Selection selection ->
            case Selection.getIfSingleElement elements selection of
                Nothing ->
                    []

                Just element ->
                    addition element

        Translate element move ->
            addition element

        Rotate element maybe ->
            addition element

        _ ->
            []


singleShapeAddition :
    (Shape -> List (Html Msg))
    -> Mode
    -> ElementSet
    -> List (Html Msg)
singleShapeAddition addition mode elements =
    case mode of
        Selection selection ->
            case selection of
                SelectShape shape ->
                    addition shape

                _ ->
                    []

        Translate element move ->
            case element.element of
                ShapeElement shape ->
                    addition shape

                _ ->
                    []

        Rotate element maybe ->
            case element.element of
                ShapeElement shape ->
                    addition shape

                _ ->
                    []

        _ ->
            []


animationButtons : Mode -> ElementSet -> List (Html Msg)
animationButtons =
    let
        buttons : Element -> List (Html Msg)
        buttons element =
            [ btn "translate"
                "Translation"
                (SetMode <| Translate element Nothing)
            , btn "rotate"
                "Rotation"
                (SetMode <| Rotate element Nothing)
            ]
    in
        singleElementAddition buttons


editShapeForms : Model -> ElementSet -> List (Html Msg)
editShapeForms model elements =
    let
        forms : Shape -> List (Html Msg)
        forms shape =
            [ div []
                [ span
                    [ class "space-lr" ]
                    [ H.text <| shapeTypeName <| Shapes.getType shape ]
                , span
                    [ class "space-lr" ]
                    [ H.text "Épaisseur" ]
                , H.input
                    [ type_ "text"
                    , HE.onInput
                        (\string ->
                            SetWidthOnShape
                                (StrokeWidth.fromString string
                                    (shape
                                        |> Shapes.getCommonProperties
                                        |> .strokeWidth
                                    )
                                )
                                shape
                        )
                    , Elements.getShape2 shape elements
                        |> Shapes.getCommonProperties
                        |> .strokeWidth
                        |> StrokeWidth.toStringRaw
                        |> HA.value
                    ]
                    []
                , span
                    [ class "space-lr" ]
                    [ H.text "x1" ]
                , H.input
                    [ type_ "text"
                    , HE.onInput
                        (\string ->
                            SetX1OnShape
                                (case emptyIsZero string of
                                    Ok float ->
                                        float + model.offset.x

                                    Err e ->
                                        shape
                                            |> correctOffset model.offset
                                            |> Shapes.getPointsArray
                                            |> p1
                                            |> .x
                                            |> (+) model.offset.x
                                )
                                shape
                        )
                    , Elements.getShape2 shape elements
                        |> correctOffset model.offset
                        |> Shapes.getPointsArray
                        |> p1
                        |> .x
                        |> toString2
                        |> HA.value
                    ]
                    []
                , span
                    [ class "space-lr" ]
                    [ H.text "y1" ]
                , H.input
                    [ type_ "text"
                    , HE.onInput
                        (\string ->
                            SetY1OnShape
                                (case emptyIsZero string of
                                    Ok float ->
                                        float + model.offset.y

                                    Err e ->
                                        shape
                                            |> correctOffset model.offset
                                            |> Shapes.getPointsArray
                                            |> p1
                                            |> .y
                                            |> (+) model.offset.y
                                )
                                shape
                        )
                    , Elements.getShape2 shape elements
                        |> correctOffset model.offset
                        |> Shapes.getPointsArray
                        |> p1
                        |> .y
                        |> toString2
                        |> HA.value
                    ]
                    []
                , span
                    [ class "space-lr" ]
                    [ H.text "x2" ]
                , H.input
                    [ type_ "text"
                    , HE.onInput
                        (\string ->
                            SetX2OnShape
                                (case emptyIsZero string of
                                    Ok float ->
                                        float + model.offset.x

                                    Err e ->
                                        shape
                                            |> correctOffset model.offset
                                            |> Shapes.getPointsArray
                                            |> p2
                                            |> .x
                                            |> (+) model.offset.x
                                )
                                shape
                        )
                    , Elements.getShape2 shape elements
                        |> correctOffset model.offset
                        |> Shapes.getPointsArray
                        |> p2
                        |> .x
                        |> toString2
                        |> HA.value
                    ]
                    []
                , span
                    [ class "space-lr" ]
                    [ H.text "y2" ]
                , H.input
                    [ type_ "text"
                    , HE.onInput
                        (\string ->
                            SetY2OnShape
                                (case emptyIsZero string of
                                    Ok float ->
                                        float + model.offset.y

                                    Err e ->
                                        shape
                                            |> correctOffset model.offset
                                            |> Shapes.getPointsArray
                                            |> p2
                                            |> .y
                                            |> (+) model.offset.y
                                )
                                shape
                        )
                    , Elements.getShape2 shape elements
                        |> correctOffset model.offset
                        |> Shapes.getPointsArray
                        |> p2
                        |> .y
                        |> toString2
                        |> HA.value
                    ]
                    []
                ]
            ]
    in
        singleShapeAddition forms model.mode elements


editAnimationForms : Model -> ElementSet -> List (Html Msg)
editAnimationForms model elements =
    let
        translationForms : Element -> Animation.Translate -> List (Html Msg)
        translationForms element (Animation.Translate translation) =
            [ span
                [ class "space-lr" ]
                [ H.text "Translation" ]
            , span
                [ class "space-lr" ]
                [ H.text "Délais" ]
            , H.input
                [ type_ "text"
                , HE.onInput
                    (\string ->
                        SetTranslationDelay
                            (case emptyIsZero string of
                                Ok float ->
                                    float
                                        |> TimeControl.timeFromFloat

                                _ ->
                                    Elements.getElement element elements
                                        |> Maybe.andThen .translation
                                        |> Maybe.map
                                            (\(Animation.Translate t) ->
                                                t.delay
                                            )
                                        |> Maybe.withDefault translation.delay
                            )
                            element
                    )
                , Elements.getElement element elements
                    |> Maybe.andThen .translation
                    |> Maybe.map (\(Animation.Translate t) -> t.delay)
                    |> Maybe.map TimeControl.timeToFloat
                    |> Maybe.withDefault 0
                    |> toString2
                    |> HA.value
                ]
                []
            , span
                [ class "space-lr" ]
                [ H.text "Durée" ]
            , H.input
                [ type_ "text"
                , HE.onInput
                    (\string ->
                        SetTranslationTime
                            (case emptyIsZero string of
                                Ok float ->
                                    float
                                        |> TimeControl.timeFromFloat

                                _ ->
                                    Elements.getElement element elements
                                        |> Maybe.andThen .translation
                                        |> Maybe.map
                                            (\(Animation.Translate t) ->
                                                t.time
                                            )
                                        |> Maybe.withDefault translation.time
                            )
                            element
                    )
                , Elements.getElement element elements
                    |> Maybe.andThen .translation
                    |> Maybe.map (\(Animation.Translate t) -> t.time)
                    |> Maybe.map TimeControl.timeToFloat
                    |> Maybe.withDefault 0
                    |> toString2
                    |> HA.value
                ]
                []
            , span
                [ class "space-lr" ]
                [ H.text "Boucle" ]
            , btn "default" "X" (SetTranslationLoop NoLoop element)
            , btn "default" "Aller" (SetTranslationLoop Restart element)
            , btn "default" "Aller-Retour" (SetTranslationLoop Return element)
            ]

        rotationForms : Element -> Animation.Rotate -> List (Html Msg)
        rotationForms element (Animation.Rotate rotation) =
            [ span
                [ class "space-lr" ]
                [ H.text "Rotation" ]
            , span
                [ class "space-lr" ]
                [ H.text "Délais" ]
            , H.input
                [ type_ "text"
                , HE.onInput
                    (\string ->
                        SetRotationDelay
                            (case emptyIsZero string of
                                Ok float ->
                                    float
                                        |> TimeControl.timeFromFloat

                                _ ->
                                    Elements.getElement element elements
                                        |> Maybe.andThen .rotation
                                        |> Maybe.map
                                            (\(Animation.Rotate t) ->
                                                t.delay
                                            )
                                        |> Maybe.withDefault rotation.delay
                            )
                            element
                    )
                , Elements.getElement element elements
                    |> Maybe.andThen .rotation
                    |> Maybe.map (\(Animation.Rotate t) -> t.delay)
                    |> Maybe.map TimeControl.timeToFloat
                    |> Maybe.withDefault 0
                    |> toString2
                    |> HA.value
                ]
                []
            , span
                [ class "space-lr" ]
                [ H.text "Durée" ]
            , H.input
                [ type_ "text"
                , HE.onInput
                    (\string ->
                        SetRotationTime
                            (case emptyIsZero string of
                                Ok float ->
                                    float
                                        |> TimeControl.timeFromFloat

                                _ ->
                                    Elements.getElement element elements
                                        |> Maybe.andThen .rotation
                                        |> Maybe.map
                                            (\(Animation.Rotate t) ->
                                                t.time
                                            )
                                        |> Maybe.withDefault rotation.time
                            )
                            element
                    )
                , Elements.getElement element elements
                    |> Maybe.andThen .rotation
                    |> Maybe.map (\(Animation.Rotate t) -> t.time)
                    |> Maybe.map TimeControl.timeToFloat
                    |> Maybe.withDefault 0
                    |> toString2
                    |> HA.value
                ]
                []
            , span
                [ class "space-lr" ]
                [ H.text "Boucle" ]
            , btn "default" "X" (SetRotationLoop NoLoop element)
            , btn "default" "Aller" (SetRotationLoop Restart element)
            , btn "default" "Aller-Retour" (SetRotationLoop Return element)
            ]

        forms : Element -> List (Html Msg)
        forms element =
            [ div []
                ([]
                    ++ (element.translation
                            |> Maybe.map (translationForms element)
                            |> Maybe.withDefault []
                       )
                    ++ (element.rotation
                            |> Maybe.map (rotationForms element)
                            |> Maybe.withDefault []
                       )
                )
            ]
    in
        singleElementAddition forms model.mode elements


shapeName : Element -> String
shapeName element =
    case element.element of
        GroupElement group ->
            "Groupe"

        ShapeElement shape ->
            Shapes.getType shape
                |> shapeTypeName


modeToSelection : Model -> Mode
modeToSelection model =
    case model.mode of
        Selection selection ->
            model.mode

        _ ->
            Selection Selection.empty


shapeSelect : String -> Model -> ShapeType -> Html Msg
shapeSelect label model shapeType =
    let
        drawing : Bool
        drawing =
            case model.mode of
                Drawing m ->
                    True

                _ ->
                    False

        style : String
        style =
            if drawing then
                if model.currentStyle.shapeType == shapeType then
                    "selected"
                else
                    "default"
            else
                "default"
    in
        btn style label (SetCurrentShape shapeType)


mapCodeView : Model -> Html Msg
mapCodeView model =
    div []
        [ H.code []
            [ model.elements
                |> modeAdjustmentsCore model.mode
                |> Elements.mapShapes (correctOffset model.offset)
                |> mapCode
                |> H.text
            ]
        ]


focusCodeView : Model -> Html Msg
focusCodeView model =
    div [ class "focus-code" ]
        [ H.code []
            [ Elements.empty
                |> addFocusElements model
                |> modeAdjustmentsCore model.mode
                |> Elements.mapShapes (correctOffset model.offset)
                |> elementsCode
                |> H.text
            ]
        ]


correctOffset : Point -> Shape -> Shape
correctOffset offset =
    updateAllPoints <| unOffsetPoint offset


unOffsetPoint : Point -> Point -> Point
unOffsetPoint offset point =
    pointDifference point offset


toPoint : Position -> Point
toPoint position =
    Point (toFloat position.x) (toFloat position.y)


btn : String -> String -> msg -> Html msg
btn style label msg =
    H.a
        [ HA.classList [ ( "btn", True ), ( "btn-" ++ style, True ) ]
        , type_ "button"
        , HE.onClick msg
        ]
        [ H.text label ]


checkBox : String -> Bool -> (Bool -> msg) -> Html msg
checkBox label state msg =
    H.label []
        [ H.input
            [ type_ "checkbox"
            , HE.onCheck msg
            , HA.checked state
            ]
            []
        , H.text label
        ]


radio : String -> String -> Bool -> msg -> Html msg
radio radioName label state msg =
    H.label []
        [ H.input
            [ type_ "radio"
            , name radioName
            , HE.onClick msg
            , HA.selected state
            ]
            []
        , H.text label
        ]
