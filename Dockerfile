FROM node:6.9

VOLUME ["/usr/src/editeur"]
ADD start.sh /start.sh
RUN chmod 755 /start.sh
CMD ["/start.sh"]
