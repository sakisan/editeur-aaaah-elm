set tabstop=4
nnoremap <leader>ct :AsyncRun ctags -R app/elm<space>

" restart "npm start" in the left pane
nnoremap <leader>ar :AsyncRun tmux send-keys -t 0 C-c Enter "npm start" Enter<cr>
